package com.ob.axita.core.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ob.axita.core.database.dao.FriendDAO;
import com.ob.axita.core.database.dao.UserDAO;
import com.ob.axita.core.pojo.Friend;

/**
 * Created by Razil on 19-Jul-18.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "user.db";
    private static final String TABLE_NAME = "USER_table";
    private static DatabaseHelper databaseHelper = null;
    private static final int DATABASE_VERSION = 1;

    //column name
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i(TAG, "Assign Database Helper");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(UserDAO.createTable());

        Log.i(TAG, "Table created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(sqLiteDatabase);
    }


}
