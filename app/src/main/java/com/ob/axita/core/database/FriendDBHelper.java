package com.ob.axita.core.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ob.axita.core.database.dao.FriendDAO;

/**
 * Created by Razil on 20-Jul-18.
 */
public class FriendDBHelper extends SQLiteOpenHelper {
    private static final String TAG = "FriendDBHelper";
    private static final String DATABASE_NAME = "friend.db";
    private static final String TABLE_NAME = "FRIEND_TABLE";
    private static FriendDBHelper friendDBHelper = null;
    private static final int DATABASE_VERSION = 2;

    public FriendDBHelper(Context context) {
        super(context, TABLE_NAME, null, DATABASE_VERSION);
        Log.i(TAG,"add database friend");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(FriendDAO.createTable());
        Log.i(TAG,"TAble Created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
