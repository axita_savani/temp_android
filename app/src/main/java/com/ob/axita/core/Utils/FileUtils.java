package com.ob.axita.core.Utils;

import android.os.Build;
import android.util.Log;

/** Created by Razil on 26/07/2018. */
public class FileUtils {
    private static String TAG = "FileUtils";

    public static String validateFileUrl(String filePath, int Picker) {
        if (Picker == 1) {
            if (!filePath.startsWith("file://")) {
                filePath = "file://" + filePath;
                Log.i(TAG,"FILE PATh FileUtils()"+filePath);
            }
        } else if (Picker == 2) {
            if (Build.VERSION.SDK_INT >= 25) {
                if (!filePath.startsWith("content://")) {
                    filePath = "content:/" + filePath;
                    Log.i(TAG,"FILE PATh FileUtils()"+filePath);
                }
            }
            else
                {
                if (!filePath.startsWith("file://")) {
                    filePath = "file://" + filePath;
                    Log.i(TAG,"FILE PATh FileUtils()"+filePath);
                }
            }
        }

        return filePath;
    }
}
