package com.ob.axita.core.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ob.axita.core.database.DatabaseHelper;
import com.ob.axita.core.pojo.User;

import java.util.ArrayList;
import java.util.List;

/** Created by Razil on 20-Jul-18. */
public class UserDAO {
    private static final String KEY_TABLE_NAME = "USER_table";

    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_NAME = "name";
    public static final String KEY_USER_ADDRESS = "address";
    public static final String KEY_USER_AGE = "age";
    public static final String KEY_USER_GENDER = "gender";
    public static final String KEY_USER_EMAIL = "email";
    public static final String KEY_USER_PHONE = "phone";
    public static final String KEY_USER_COMPANY = "company";
    public static final String KEY_USER_BALANCE = "balance";
    public static final String KEY_USER_EYECOLOR = "eyecolor";
    public static final String KEY_ABOUT = "about";
    private static final String TAG = "UserDAO";
    private static final String USER_JSON = "user_json";
    private static final String KEY_FRIEND_TABLE_NAME = "FRIEND_table";
    private Context context;
    DatabaseHelper databaseHelper;
    FriendDAO friendDAO;

    User user;

    public UserDAO(Context context) {
        this.context = context;
        databaseHelper = new DatabaseHelper(context);
        friendDAO = new FriendDAO(context);
        user = new User();
    }

    public static String createTable() {
        Log.i(TAG, "carete table");
        return "CREATE TABLE "
                + KEY_TABLE_NAME
                + "("
                + KEY_USER_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_USER_NAME
                + " TEXT,"
                + KEY_USER_ADDRESS
                + " TEXT,"
                + KEY_USER_AGE
                + " TEXT,"
                + KEY_USER_GENDER
                + " TEXT,"
                + KEY_USER_EMAIL
                + " TEXT,"
                + KEY_USER_PHONE
                + " TEXT,"
                + KEY_USER_COMPANY
                + " TEXT,"
                + KEY_USER_BALANCE
                + " TEXT,"
                + KEY_USER_EYECOLOR
                + " TEXT)";
    }

    public long insertuser(User user) {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_NAME, user.getName());
        values.put(KEY_USER_ADDRESS, user.getAddress());
        values.put(KEY_USER_AGE, user.getAge());
        values.put(KEY_USER_GENDER, user.getGender());
        values.put(KEY_USER_EMAIL, user.getEmail());
        values.put(KEY_USER_PHONE, user.getPhone());
        values.put(KEY_USER_COMPANY, user.getCompany());
        values.put(KEY_USER_BALANCE, user.getBalance());
        values.put(KEY_USER_EYECOLOR, user.getEyeColor());
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        Log.i(TAG, "inserted USer");
        return database.insert(KEY_TABLE_NAME, null, values);
    }

    public long deleteUser(Integer id) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        return database.delete(KEY_TABLE_NAME, "id=?", new String[] {Integer.toString(id)});
    }

    public Cursor getData() {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor res = database.rawQuery("select * from USER_table", null);
        return res;
    }

    public User UserGet(long data_id) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor =
                database.rawQuery(
                        "select * from USER_table where id=?",
                        new String[] {String.valueOf(data_id)});
        User user = new User();
        if (cursor != null && cursor.moveToFirst()) {
            user.setName(cursor.getString(cursor.getColumnIndex(KEY_USER_NAME)));
            user.setAddress(cursor.getString(cursor.getColumnIndex(KEY_USER_ADDRESS)));
            user.setAge(cursor.getString(cursor.getColumnIndex(KEY_USER_AGE)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(KEY_USER_EMAIL)));
            user.setPhone(cursor.getString(cursor.getColumnIndex(KEY_USER_PHONE)));
            user.setCompany(cursor.getString(cursor.getColumnIndex(KEY_USER_COMPANY)));
            user.setBalance(cursor.getString(cursor.getColumnIndex(KEY_USER_BALANCE)));
            user.setEyeColor(cursor.getString(cursor.getColumnIndex(KEY_USER_EYECOLOR)));
            user.setGender(cursor.getString(cursor.getColumnIndex(KEY_USER_GENDER)));
            cursor.close();
        }

        return user;
    }

    public List<User> getAllUser() {
        List<User> userList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + KEY_TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_USER_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(KEY_USER_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(KEY_USER_EMAIL)));
                // Adding  to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        Log.i(TAG, "size" + userList);
        // return  list
        return userList;
    }

    public List<User> getFriendList(long id) {
        List<User> userlist = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Log.i(TAG, "getFriendList()");
        List<String> list = friendDAO.getAddfriednList(id);
        Log.i(TAG, "getAddfriednList()" + list);
        Log.i(TAG, "getFriendList()-->Array Friend" + list.toString());
        String friend = list.toString();
        friend = friend.replace("[", "(");
        friend = friend.replace("]", ")");
        Log.i(TAG, "getAddfriednList()" + friend);
        String str =
                "select * from " + KEY_TABLE_NAME + " where " + KEY_USER_ID + " not in " + friend;
        Log.i(TAG, "Select-->" + str);

        String query = "select * from " + KEY_TABLE_NAME + " where not " + KEY_USER_ID + " =?";
        Cursor cursor = db.rawQuery(str, null);
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_USER_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(KEY_USER_NAME)));
                Log.e(TAG, "getFriendList()" + user.getId());
                Log.e(TAG, "getFriendList()" + user.getName());
                userlist.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return userlist;
    }
}
