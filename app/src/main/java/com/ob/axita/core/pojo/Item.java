package com.ob.axita.core.pojo;

/**
 * Created by Razil on 19-Jun-18.
 */
public class Item {
    String name;
    String subname;

    int c;

    public Item(String name, String subname, int c) {
        this.name = name;
        this.subname = subname;

        this.c = c;

    }

    public String getName() {
        return name;
    }

    public String getSubname() {
        return subname;
    }


    public int getC() {
        return c;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }


    public Item setSubname(String subname) {
        this.subname = subname;
        return this;
    }

    public Item setC(int c) {
        this.c = c;
        return this;
    }
}
