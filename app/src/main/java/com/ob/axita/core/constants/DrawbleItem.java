package com.ob.axita.core.constants;

/** Created by Razil on 25-Jun-18. */
public class DrawbleItem {
    public static final int ITEM_LISTVIEW = 0;
    public static final int ITEM_RECYCLERVIEW = 1;
    public static final int ITEM_DRAWBLES = 2;
    public static final int ITEM_LISTVIEW_DISPLAY = 3;
    public static final int ITEM_LIST_VIEW_SELECT = 4;
    public static final int ITEM_CONTEXT_MENU_BUTTON = 5;
    public static final int ITEM_CONTEXT_MENU_LISTVIEW = 6;
    public static final int ITEM_POPUP_MENU = 7;
    public static final int ITEM_DIALO_ALERT = 8;
    public static final int ITEM_DIALOG_MULTICHOICE = 9;
    public static final int ITEM_DIALOG_DATEPICKER = 10;
    public static final int ITEM_DIALOG_CUSTOM = 11;
    public static final int ITEM_JSON_EXMAPLE = 12;
    public static final int ITEM_DATABASE_SQL = 13;
    public static final int ITEM_CAMERA_PICKER = 14;
    public static final int ITEM_VIDEO_PICKER = 15;
    public static final int ITEM_FILE_PICKER = 16;
}
