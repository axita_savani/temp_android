package com.ob.axita.core.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ob.axita.core.database.FriendDBHelper;
import com.ob.axita.core.pojo.User;

import java.util.ArrayList;
import java.util.List;

/** Created by Razil on 20-Jul-18. */
public class FriendDAO {
    private static final String KEY_USER_TABLE = "USER_table";
    private static final String KEY_TABLE_NAME = "FRIEND_table";
    private static final String TAG = "FriendDAO";
    public static final String KEY_ID = "id1";
    public static final String KEY_USER_ID = "userID";
    public static final String KEY_USER_FRIEND_NAME = "friendname";
    public static final String KEY_FRIEND_ID = "friendID";
    public static final String KEY_USERTABLE_ID = "id";
    private Context context;
    FriendDBHelper friendDBHelper;

    public FriendDAO(Context context) {
        this.context = context;
        friendDBHelper = new FriendDBHelper(context);
    }

    public static String createTable() {
        Log.i(TAG, "create table");
        final String TABLE_CREATE =
                "create table "
                        + KEY_TABLE_NAME
                        + " ("
                        + KEY_ID
                        + " integer primary key autoincrement, "
                        + KEY_FRIEND_ID
                        + " integer ,"
                        + KEY_USER_FRIEND_NAME
                        + " text ,"
                        + KEY_USER_ID
                        + " integer ,"
                        + " FOREIGN KEY ("
                        + KEY_USER_ID
                        + ") REFERENCES "
                        + KEY_USER_TABLE
                        + "("
                        + KEY_USERTABLE_ID
                        + "));";
        return TABLE_CREATE;
    }

    public long insertFriendData(long userID, long friendID, String username) {
        ContentValues values = new ContentValues();
        values.put(KEY_FRIEND_ID, friendID);
        values.put(KEY_USER_ID, userID);
        values.put(KEY_USER_FRIEND_NAME, username);
        SQLiteDatabase database = friendDBHelper.getWritableDatabase();
        return database.insert(KEY_TABLE_NAME, null, values);
    }

    public List<String> getAddfriednList(long id) {
        List<User> friendlist = new ArrayList<>();
        List<String> array= new ArrayList<>();
        SQLiteDatabase db = friendDBHelper.getWritableDatabase();
        Log.i(TAG, "getAddfriednList()");
        String str =
                "select "
                        + KEY_USER_ID
                        + ","
                        + KEY_USER_FRIEND_NAME
                        + " from "
                        + KEY_TABLE_NAME
                        + " where "
                        + KEY_FRIEND_ID
                        + " =?";

        Cursor cursor = db.rawQuery(str, new String[] {String.valueOf(id)});
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_USER_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(KEY_USER_FRIEND_NAME)));
                Log.e(TAG, "getAddfriednList() data-->" + user.getId());
                friendlist.add(user);
                array.add(String.valueOf(user.getId()));
            } while (cursor.moveToNext());

        }
        array.add(String.valueOf(id));
        return array;
    }

    public void deletefriend() {
        SQLiteDatabase database = friendDBHelper.getWritableDatabase();
        database.execSQL("delete from " + KEY_TABLE_NAME);
        Log.i(TAG, "deleted");
    }

    public List<User> getUSERID(long frienID) {
        List<User> list = new ArrayList<>();
        Log.i(TAG, "getUSERID()" + frienID);
        SQLiteDatabase db = friendDBHelper.getReadableDatabase();
        String str =
                "select "
                        + KEY_USER_ID
                        + ","
                        + KEY_USER_FRIEND_NAME
                        + " from "
                        + KEY_TABLE_NAME
                        + " where "
                        + KEY_FRIEND_ID
                        + " =?";

        Cursor cursor = db.rawQuery(str, new String[] {String.valueOf(frienID)});

        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(cursor.getLong(cursor.getColumnIndex(KEY_USER_ID)));
                user.setName(cursor.getString(cursor.getColumnIndex(KEY_USER_FRIEND_NAME)));
                list.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }
}
