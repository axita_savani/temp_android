package com.ob.axita.core.constants;

/** Created by Razil on 21-Jun-18. */
public class Constants {
    public static final long SPLASH_SCREEN_TIME = 2000;
    public static final String TAB_POSITION = "tab_position";
    public static final int STORAGE_PERMISSION = 23;
    public static final int SCREEN_CLOSE_TIME = 500;
    public static final int EXTERNAL_STORAGE_PUBLIC_DIR = 100;
    public static final int EXTERNAL_STORAGE_APP_DIR = 200;
    public static final int EXTERNAL_CACHE_DIR = 300;
    public static final int INTERNAL_APP_DIR = 400;
}
