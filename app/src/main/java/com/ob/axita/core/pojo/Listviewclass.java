package com.ob.axita.core.pojo;

/**
 * Created by Razil on 18-Jun-18.
 */
public class Listviewclass {
    public String fname;
    private boolean isSelected;

    public Listviewclass(String fname) {
        super();
        this.fname = fname;
    }

    public Listviewclass() {
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean Selected) {
        isSelected = Selected;
    }

}
