package com.ob.axita.core.constants;

//////////////////////////////////////////////////////////////////////////////
//                   OptimumBrew Technology Pvt. Ltd.                       //
//                                                                          //
// Title:            DeremaHelp                                     //
// File:             com.derema.deremahelp.core.constants                                       //
// Since:            11-August-2016          //
//                                                                          //
// Author:           Krishna Ghanva                                         //
// Email:            krishna.ghanva@optimumbrew.com                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

public class Extras {
    public static final String EXTRA_FRAGMENT_SIGNUP = "EXTRA_FRAGMENT_SIGNUP";
    public static final String EXTRA_FRAGMENT_BUNDLE = "bundle";

    public static final String EXTRA_BRAND = "USer";

    public static final String EXTRA_ID = "id_user";
    /**
     * Cache param tag
     */
    public static final String PAGE_NUMBER = "page_number";
    public static final String API_NAME = "api_name";
    public static final String REQUEST_JSON = "request_json";

    public static final String EXTRA_JUMP_TO = "jump_to";
}
