package com.ob.axita.ui.activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.ob.axita.R;
import com.ob.axita.core.constants.DrawbleItem;
import com.ob.axita.ui.fragment.CamerePickerFragment;
import com.ob.axita.ui.fragment.ContextmenuButtonFragment;
import com.ob.axita.ui.fragment.ContextmenuListviewFragment;
import com.ob.axita.ui.fragment.DialogAlertFragment;
import com.ob.axita.ui.fragment.DialogCustomInputFragment;
import com.ob.axita.ui.fragment.DialogDatepickerFragment;
import com.ob.axita.ui.fragment.DialogMultiChoiceFragment;
import com.ob.axita.ui.fragment.DrawblesFragment;
import com.ob.axita.ui.fragment.FilePickerFragment;
import com.ob.axita.ui.fragment.JsonDemoFragment;
import com.ob.axita.ui.fragment.ListViewContactFragment;
import com.ob.axita.ui.fragment.ListViewSelectFragment;
import com.ob.axita.ui.fragment.PopupMenuFragment;
import com.ob.axita.ui.fragment.RcyclerContackFragment;
import com.ob.axita.ui.fragment.RecyclerViewDisplayFragment;
import com.ob.axita.ui.fragment.SqlDatabaseFragment;
import com.ob.axita.ui.fragment.VideoPickerFragment;
import com.ob.axita.ui.interfaces.HostInterface;

import static com.ob.axita.core.constants.Constants.STORAGE_PERMISSION;

/** Created by Razil on 25-Jun-18. */
public class MainActivityNew extends AppCompatActivity
        implements HostInterface, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivityNew";
    private android.support.v7.widget.Toolbar toolbar;
    private android.widget.FrameLayout layoutFHostFragment;
    private android.support.design.widget.NavigationView navigationdemo;
    private android.support.v4.widget.DrawerLayout drawerlayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ConnectivityManager connectivityManager;
    private TextView textView;
    private JsonObjectRequest jsonObjectRequest;
    private boolean flag, flag2;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_new);

        this.drawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.navigationdemo = (NavigationView) findViewById(R.id.navigation_demo);
        this.layoutFHostFragment = (FrameLayout) findViewById(R.id.layoutFHostFragment);
        this.toolbar = (Toolbar) findViewById(R.id.tool_bar);

        flag = isExternalStorageWritable();
        if (flag) {
            // Toast.makeText(getApplicationContext(), "External Storage write Only",
            // Toast.LENGTH_LONG).show();

        } else {
            // Toast.makeText(getApplicationContext(), "External Storage required Permission",
            // Toast.LENGTH_LONG).show();
        }
        flag2 = isExternalStorageReaable();
        if (flag) {
            // Toast.makeText(getApplicationContext(), "External Storage Read only",
            // Toast.LENGTH_LONG).show();
        } else {
            //  Toast.makeText(getApplicationContext(), "External Storage write onlys",
            // Toast.LENGTH_LONG).show();
        }
        connectionCheck();

        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);

        mDrawerToggle =
                new ActionBarDrawerToggle(
                        this,
                        drawerlayout,
                        toolbar,
                        R.string.app_name,
                        R.string.abc_action_bar_home_description) {

                    /** Called when a drawer has settled in a completely closed state. */
                    public void onDrawerClosed(View view) {
                        super.onDrawerClosed(view);
                        // getActionBar().setTitle(mTitle);

                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                        /* if(isEnableSearch()){
                            enableSearchInToolbar(true);
                        }*/
                    }

                    /** Called when a drawer has settled in a completely open state. */
                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                        toolbar.setNavigationIcon(R.drawable.back38);
                        // getActionBar().setTitle(d);
                        // hideKeyBoard();
                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }
                };

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        mDrawerToggle.syncState();
        navigationdemo.setNavigationItemSelectedListener(this);
    }

    private boolean isExternalStorageReaable() {
        String str = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(str)
                || Environment.MEDIA_MOUNTED_READ_ONLY.equals(str)) {
            return true;
        }
        return false;
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        Log.i(TAG, "External Storage" + state);
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
    /*check file external and internal storage avalible or not*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();

        drawerlayout.closeDrawers();
        Log.i(TAG, "itemid" + itemId);
        switch (itemId) {
            case R.id.navigation_list_view:
                changeCurrentFragmentTo(DrawbleItem.ITEM_LISTVIEW, null);

                break;
            case R.id.navigation_recycler_view:
                changeCurrentFragmentTo(DrawbleItem.ITEM_RECYCLERVIEW, null);
                break;
            case R.id.navigation_drawbles:
                changeCurrentFragmentTo(DrawbleItem.ITEM_DRAWBLES, null);
                break;
            case R.id.navigation_list_view_display:
                changeCurrentFragmentTo(DrawbleItem.ITEM_LISTVIEW_DISPLAY, null);
                break;
            case R.id.navigation_list_view_select:
                changeCurrentFragmentTo(DrawbleItem.ITEM_LIST_VIEW_SELECT, null);
                break;
            case R.id.navigation_context_menu_button:
                changeCurrentFragmentTo(DrawbleItem.ITEM_CONTEXT_MENU_BUTTON, null);
                break;
            case R.id.navigation_context_menu_listview:
                changeCurrentFragmentTo(DrawbleItem.ITEM_CONTEXT_MENU_LISTVIEW, null);
                break;
            case R.id.navigation_popup_menu:
                changeCurrentFragmentTo(DrawbleItem.ITEM_POPUP_MENU, null);
                break;
            case R.id.navigation_dialog_alert:
                changeCurrentFragmentTo(DrawbleItem.ITEM_DIALO_ALERT, null);
                break;
            case R.id.navigation_dialog_multichoice:
                changeCurrentFragmentTo(DrawbleItem.ITEM_DIALOG_MULTICHOICE, null);
                break;
            case R.id.navigation_dialog_datepicker:
                changeCurrentFragmentTo(DrawbleItem.ITEM_DIALOG_DATEPICKER, null);
                break;
            case R.id.navigation_dialog_custom:
                changeCurrentFragmentTo(DrawbleItem.ITEM_DIALOG_CUSTOM, null);
                break;
            case R.id.navigation_json:
                changeCurrentFragmentTo(DrawbleItem.ITEM_JSON_EXMAPLE, null);
                break;
            case R.id.navigation_sql:
                changeCurrentFragmentTo(DrawbleItem.ITEM_DATABASE_SQL, null);
                break;
            case R.id.navigation_camerapicker:
                changeCurrentFragmentTo(DrawbleItem.ITEM_CAMERA_PICKER, null);
                break;
            case R.id.navigation_videopicker:
                changeCurrentFragmentTo(DrawbleItem.ITEM_VIDEO_PICKER,null);
                break;
            case R.id.navigation_filepicker:
                changeCurrentFragmentTo(DrawbleItem.ITEM_FILE_PICKER,null);
                break;
        }
        return true;
    }

    @Override
    public void enableSearchInToolbar(boolean status) {}

    @Override
    public void setSearchHint(int hindId) {}

    @Override
    public void refreshNavigationDrawer() {}

    @Override
    public void changeCurrentFragmentTo(int currentFragmentId, Bundle bundle) {
        switch (currentFragmentId) {
            case 0:
                ListViewContactFragment listViewContactFragment = new ListViewContactFragment();
                ChangeCurrentFragment(listViewContactFragment);

                break;
            case 1:
                RcyclerContackFragment rcyclerContackFragment = new RcyclerContackFragment();
                ChangeCurrentFragment(rcyclerContackFragment);
                break;
            case 2:
                DrawblesFragment drawblesFragment = new DrawblesFragment();
                ChangeCurrentFragment(drawblesFragment);
                break;
            case 3:
                RecyclerViewDisplayFragment listViewDisplayFragment =
                        new RecyclerViewDisplayFragment();
                ChangeCurrentFragment(listViewDisplayFragment);
                break;
            case 4:
                ListViewSelectFragment listViewSelectFragment = new ListViewSelectFragment();
                ChangeCurrentFragment(listViewSelectFragment);
                break;
            case 5:
                ContextmenuButtonFragment contextmenuButtonFragment =
                        new ContextmenuButtonFragment();
                ChangeCurrentFragment(contextmenuButtonFragment);
                break;
            case 6:
                ContextmenuListviewFragment contextmenuListviewFragment =
                        new ContextmenuListviewFragment();
                ChangeCurrentFragment(contextmenuListviewFragment);
                break;
            case 7:
                PopupMenuFragment popupMenuFragment = new PopupMenuFragment();
                ChangeCurrentFragment(popupMenuFragment);
                break;
            case 8:
                DialogAlertFragment dialogAlertFragment = new DialogAlertFragment();
                ChangeCurrentFragment(dialogAlertFragment);
                break;
            case 9:
                DialogMultiChoiceFragment dialogMultiChoiceFragment =
                        new DialogMultiChoiceFragment();
                ChangeCurrentFragment(dialogMultiChoiceFragment);
                break;
            case 10:
                DialogDatepickerFragment datepickerFragment = new DialogDatepickerFragment();
                ChangeCurrentFragment(datepickerFragment);
                break;
            case 11:
                DialogCustomInputFragment dialogCustomInputFragment =
                        new DialogCustomInputFragment();
                ChangeCurrentFragment(dialogCustomInputFragment);
                break;
            case 12:
                JsonDemoFragment jsonDemoFragment = new JsonDemoFragment();
                ChangeCurrentFragment(jsonDemoFragment);
                break;
            case 13:
                SqlDatabaseFragment sqlDatabaseFragment = new SqlDatabaseFragment();
                ChangeCurrentFragment(sqlDatabaseFragment);
                break;
            case 14:
                CamerePickerFragment camerePickerFragment = new CamerePickerFragment();
                ChangeCurrentFragment(camerePickerFragment);
                break;
            case 15:
                VideoPickerFragment videoPickerFragment = new VideoPickerFragment();
                ChangeCurrentFragment(videoPickerFragment);
                break;
            case 16:
                FilePickerFragment filePickerFragment=new FilePickerFragment();
                ChangeCurrentFragment(filePickerFragment);
                break;
        }
    }

    /** Set current fragment */
    private void ChangeCurrentFragment(Fragment fragment) {
        // hideKeyBoard();
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        fTransaction.replace(R.id.layoutFHostFragment, fragment, fragment.getClass().getName());
        fTransaction.commitAllowingStateLoss();
    }

    @Override
    public void setToolbarTitle(int resId) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(resId);
    }

    @Override
    public void changeDrawerItem(int drawerItemPosition) {}

    @Override
    public void setToolbarTitle(String format) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(format);
        }
    }

    @Override
    public void setTabSelection(int tabPosition) {}

    // -----------------------------------------OPtion menu
    // creted---------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_optionmenu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menuSearch:
                Toast.makeText(this, "Search option selected", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuSetting:
                Toast.makeText(this, "Setting option selected", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuProfile:
                Toast.makeText(this, "Profile option selected", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuAccount:
                Toast.makeText(this, "Account option selected", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void connectionCheck() {
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean iswifi = networkInfo.isConnected();
        // boolean isroming = networkInfo.isRoaming();

        NetworkInfo networkInfo1 =
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean ismobile = networkInfo1.isConnected();
        // boolean ismo = networkInfo1.isFailover();
        Log.i(TAG, "Wifi--" + iswifi);
        Log.i(TAG, "Mobile" + ismobile);
        if (iswifi) {
            //   Toast.makeText(this, "Connection Successfully With Wifi !!  ",
            // Toast.LENGTH_LONG).show();
        } else {
            if (ismobile) {
                //    Toast.makeText(this, "Connection Successfully With Mobile Data!!  ",
                // Toast.LENGTH_LONG).show();
            } else {
                //  Toast.makeText(this, "Connection Failed  ", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String permissions[], int[] grantResults) {
        Log.i(TAG, "grant result-->" + grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
