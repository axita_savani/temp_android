package com.ob.axita.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.ob.axita.R;
import com.ob.axita.core.pojo.Listviewclass;
import com.ob.axita.ui.adapter.MyBaseAdapter;
import com.ob.axita.ui.interfaces.HostInterface;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Razil on 25-Jun-18.
 */
public class ListViewSelectFragment extends Fragment implements View.OnClickListener {


    String friend[] = new String[]{"Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek"};
    ListView listViewfrd;
    public MyBaseAdapter adpt1;
    ArrayList<Listviewclass> arrayList = new ArrayList<>();
    EditText editText;
    private static final String TAG = "ListViewSelectFragment";
    Context context;
    private HostInterface hostInterface;
    private FloatingActionButton fab;
    private static final int RESQUEST_CODE = 42;
    String str;



    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        hostInterface = (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context = getContext();
        hostInterface.setToolbarTitle("List View Selected");
        Log.i(TAG,"Create");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG," onCreateView");
        View view = inflater.inflate(R.layout.fragment_list_select, container, false);

        this.listViewfrd = view.findViewById(R.id.listfriend);
        this.listViewfrd.setTextFilterEnabled(true);

        getaDataInList(false);


        this.editText = view.findViewById(R.id.edittext_search);
        this.fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.i(TAG,"ViewCreate");
        adpt1 = new MyBaseAdapter(context, arrayList);
        listViewfrd.setAdapter(adpt1);
        adpt1.notifyDataSetChanged();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                // Log.i("LoginActivity", "call bak to adapter get string");
                adpt1.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        super.onViewCreated(view, savedInstanceState);
    }


    private ArrayList<Listviewclass> getaDataInList(boolean isSelected) {

        friend[0]=str;
        Log.i(TAG, "String"+friend[0]);
        ArrayList<Listviewclass> list = new ArrayList<>();
        for (int i = 0; i < friend.length; i++) {

            Listviewclass ld = new Listviewclass();
            ld.setSelected(isSelected);
            ld.setFname(friend[i]);
            arrayList.add(ld);

          //  Log.i(TAG, "get data calling");
        }
        return list;
    }



    @Override
    public void onClick(View view) {
        EnterFriendListviewFragment enterFriendListviewFragment = new EnterFriendListviewFragment();

        Log.i(TAG, "click on fab" + view.getId());
        switch (view.getId()) {

            case R.id.fab:
                Log.i(TAG, "click on fab");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                enterFriendListviewFragment.setTargetFragment(this, RESQUEST_CODE);
                fragmentTransaction.replace(R.id.layoutFHostFragment, enterFriendListviewFragment);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivity Call");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Log.i(TAG, "result ok");
                    Log.i(TAG, "Data catced " + data.getStringExtra("Friend"));
                    str = data.getStringExtra("Friend");
                    getaDataInList(false);
                    Log.i(TAG, "Call function");



                }
        }
    }

}
