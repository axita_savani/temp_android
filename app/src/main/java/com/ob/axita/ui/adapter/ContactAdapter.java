package com.ob.axita.ui.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.core.pojo.Item;
import com.ob.axita.ui.fragment.ListViewContactFragment;

import java.util.ArrayList;

/**
 * Created by Razil on 19-Jun-18.
 */
public class ContactAdapter extends ArrayAdapter<Item> {
    private static final String TAG = "ContactAdapter";
    private ArrayList<Item> arrayset;
 private     Context mcontext;
    //GradientDrawable draw = new GradientDrawable();


    public ContactAdapter(ArrayList<Item> data, Context context) {
        super(context, R.layout.contact_list_view, data);
        this.arrayset = data;
        this.mcontext = context;

    }



    private static class ViewHolder {
        TextView fname;
        TextView surname;
        TextView str;

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final View result;
        Item item = getItem(position);
        ViewHolder viewHolder;


        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.contact_list_view, parent, false);
            viewHolder.fname = (TextView) convertView.findViewById(R.id.txtcname);
            viewHolder.surname = (TextView) convertView.findViewById(R.id.txtsurname);
            viewHolder.str = (TextView) convertView.findViewById(R.id.txtcfirst);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
            Log.i(TAG,"else");
            Log.i(TAG, "" + item.getC());
           // draw.setColor(item.getC());


        }

        viewHolder.fname.setText(item.getName());
        viewHolder.surname.setText(item.getSubname());
        viewHolder.str.setText(item.getName());
       // draw.setShape(GradientDrawable.OVAL);

        viewHolder.str.setBackgroundColor(item.getC());
        return convertView;

    }
}
