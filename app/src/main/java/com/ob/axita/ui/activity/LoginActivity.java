package com.ob.axita.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.ob.axita.ui.adapter.MyBaseAdapter;
import com.ob.axita.R;
import com.ob.axita.core.pojo.Listviewclass;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
     ListView listViewfrd;
    Context context = LoginActivity.this;
    EditText editTextSearch;
    Button btnfrd;
    Button btncon;
    Button btnfav;
    private static final String TAG = "LoginActivity";
    public MyBaseAdapter adpt1;
    Toolbar toolbarlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        MyBaseAdapter nb;
        btnfrd = findViewById(R.id.buttonfrd);
        btncon=findViewById(R.id.btncontact);
        btnfav=findViewById(R.id.btnfavorite);
        listViewfrd = findViewById(R.id.listfriend);
        editTextSearch = findViewById(R.id.searchtxt);
        toolbarlogin=findViewById(R.id.toolbar);
        toolbarlogin.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        listViewfrd.setTextFilterEnabled(true);

     //   getaDataInList(false);
        Log.i("LoginActivity", "get data called");

        btnfrd.setOnClickListener(this);
        btncon.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonfrd) {
           // Intent intent = new Intent(LoginActivity.this, DisplayDataActivity.class);
          //  startActivity(intent);
        }


    }



}
