package com.ob.axita.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ob.axita.ui.fragment.ListViewContactFragment;
import com.ob.axita.ui.fragment.DrawblesFragment;
import com.ob.axita.ui.fragment.RcyclerContackFragment;

/**
 * Created by Razil on 22-Jun-18.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        if(position==0)
        {
            fragment=new ListViewContactFragment();
        }
        else if(position==1)
        {
            fragment=new DrawblesFragment();
        }
        else if(position==2)
        {
            fragment=new RcyclerContackFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title=null;
        if(position==0)
        {
            title="Home";
        }
        else if(position==1)
        {
            title="Like";
        }
        else if(position==2)
        {
            title="Search";
        }
        return title;
    }
}
