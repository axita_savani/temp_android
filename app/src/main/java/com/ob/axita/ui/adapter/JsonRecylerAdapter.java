package com.ob.axita.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ob.axita.R;

import java.util.ArrayList;

/**
 * Created by Razil on 30-Jun-18.
 */
public class JsonRecylerAdapter extends RecyclerView.Adapter<JsonRecylerAdapter.ViewHolder> {
    ArrayList<String> id;
    ArrayList<String> name;
    ArrayList<String> address;
    ArrayList<String> city;
    ArrayList<String> phone;
    Context context;

    public JsonRecylerAdapter(ArrayList<String> id, ArrayList<String> name, ArrayList<String> address, ArrayList<String> city, ArrayList<String> phone, Context context) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.phone = phone;
        this.context = context;
    }

    @NonNull
    @Override
    public JsonRecylerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.json_listview_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JsonRecylerAdapter.ViewHolder holder, int position) {
        holder.id.setText(id.get(position));
        holder.name.setText(name.get(position));
        holder.address.setText(address.get(position));
        holder.city.setText(city.get(position));
        holder.phone.setText(phone.get(position));

    }

    @Override
    public int getItemCount() {
        return id.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, name, city, address, phone;

        public ViewHolder(View itemView) {

            super(itemView);
            id = itemView.findViewById(R.id.json_id);
            name = itemView.findViewById(R.id.json_name);
            city = itemView.findViewById(R.id.json_city);
            address = itemView.findViewById(R.id.json_address);
            phone = itemView.findViewById(R.id.json_phone);
        }
    }
}
