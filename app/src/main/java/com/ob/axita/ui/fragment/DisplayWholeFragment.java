package com.ob.axita.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.core.constants.Extras;
import com.ob.axita.core.database.dao.FriendDAO;
import com.ob.axita.core.database.dao.UserDAO;
import com.ob.axita.core.pojo.User;
import com.ob.axita.ui.activity.BaseFragmentActivity;
import com.ob.axita.ui.adapter.SelectedFriendAdapter;

import java.util.List;

/** Created by Razil on 20-Jul-18. */
public class DisplayWholeFragment extends BaseFragment {
    private static final String TAG = "DisplayWholeFragment";
    private static final int RESQUEST_CODE = 125;
    private Context context;
    private User user;
    private TextView name, gender, age, eyecolor, address, phone, email, company, balance, about;
    private ImageView addFriend;
    UserDAO userDAO;
    private RecyclerView displayfriend;
    SelectedFriendAdapter selectedFriendAdapter;
    FriendDAO friendDAO;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userDAO = new UserDAO(context);
        friendDAO = new FriendDAO(context);
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sqldisplay_user, container, false);
        this.name = view.findViewById(R.id.txtName);
        this.age = view.findViewById(R.id.txtAge);
        this.gender = view.findViewById(R.id.txtGender);
        this.eyecolor = view.findViewById(R.id.txtEyeColor);
        this.address = view.findViewById(R.id.txtAddress);
        this.phone = view.findViewById(R.id.txtPhone);
        this.email = view.findViewById(R.id.txtEmail);
        this.company = view.findViewById(R.id.txtCompany);
        this.balance = view.findViewById(R.id.txtBalance);
        this.about = view.findViewById(R.id.txtAbout);
        this.addFriend = view.findViewById(R.id.imgFriendAdd);

        this.displayfriend = view.findViewById(R.id.RecylerDisplayFriend);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        displayfriend.setLayoutManager(llm);

        ///////////////// Recyler View///////////////
        final Bundle bundle = getArguments();

        assert bundle != null;
        final User user1 = (User) bundle.getSerializable(Extras.EXTRA_BRAND);
        assert user != null;
        assert user1 != null;
        Log.i(TAG, "bundle-->" + user1.getId());
        long str = user1.getId();

        user = userDAO.UserGet(str);
        setToolbarTitle(user.getName());

        name.setText(user.getName() != null && user.getName().length() > 0 ? user.getName() : "");
        gender.setText(
                user.getGender() != null && user.getGender().length() > 0 ? user.getGender() : "");
        age.setText(user.getAge() != null && user.getAge().length() > 0 ? user.getAge() : "");
        eyecolor.setText(
                user.getEyeColor() != null && user.getEyeColor().length() > 0
                        ? user.getEyeColor()
                        : "");
        address.setText(
                user.getAddress() != null && user.getAddress().length() > 0
                        ? user.getAddress()
                        : "");
        phone.setText(
                user.getPhone() != null && user.getPhone().length() > 0 ? user.getPhone() : "");
        email.setText(
                user.getEmail() != null && user.getEmail().length() > 0 ? user.getEmail() : "");
        company.setText(
                user.getCompany() != null && user.getCompany().length() > 0
                        ? user.getCompany()
                        : "");
        balance.setText(
                user.getBalance() != null && user.getBalance().length() > 0
                        ? user.getBalance()
                        : "");
        about.setText(
                user.getAbout() != null && user.getAbout().length() > 0 ? user.getAbout() : "");

        List<User> list = friendDAO.getUSERID(user1.getId());
        Log.i(TAG, "getUSERID()");

        for (User userid : list) {
            String log =
                    "USer ID:"
                            + user1.getId()
                            + " ,Id: "
                            + userid.getId()
                            + " ,Name: "
                            + userid.getName();
            Log.i(TAG, " Userdata-->" + log);
        }
        Log.i(TAG, "getUSERID()");

        selectedFriendAdapter =
                new SelectedFriendAdapter(getContext(), friendDAO.getUSERID(user1.getId()));
        Log.i(TAG, "selectedFriendAdapter ");
        displayfriend.setAdapter(selectedFriendAdapter);
        addFriend.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Log.i(TAG, "Click on add friend button-->" + user1.getId());

                        Intent intent = new Intent(context, BaseFragmentActivity.class);
                        ChooseFriendFragment chooseFriendFragment = new ChooseFriendFragment();

                        ///////// Add Adapter into Recyler View/////////////////////
                      /*  List<User> list = friendDAO.getAddfriednList(user.getId());
                        Log.i(TAG, "getAddfriednList()");
                        for (User user2 : list) {
                            String log =
                                    "USer ID:"
                                            + user1.getId()
                                            + "Id: "
                                            + user2.getId()
                                            + "Name:"
                                            + user2.getName();
                            Log.i(TAG, "getAddfriednList-->" + log);
                        }
                      */  Bundle bundle = new Bundle();
                        bundle.putSerializable(Extras.EXTRA_ID, user1);

                        Log.i(TAG, "Bundle-->" + user1.getId());
                        intent.putExtra(Extras.EXTRA_FRAGMENT_BUNDLE, bundle);
                        intent.putExtra(Extras.EXTRA_FRAGMENT_SIGNUP, chooseFriendFragment);

                        context.startActivity(intent);
                        Log.i(TAG, "choose fragment");
                    }
                });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = getArguments();
        final User user1 = (User) bundle.getSerializable(Extras.EXTRA_BRAND);
        selectedFriendAdapter =
                new SelectedFriendAdapter(getContext(), friendDAO.getUSERID(user1.getId()));
        Log.i(TAG, "SelectedFriendAdapte onResume()" + user1.getId());
        displayfriend.setAdapter(selectedFriendAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public BaseFragment createFragment(Bundle bundle) {
        return null;
    }
}
