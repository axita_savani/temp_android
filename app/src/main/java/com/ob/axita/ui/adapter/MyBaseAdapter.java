package com.ob.axita.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;

import com.ob.axita.R;
import com.ob.axita.core.pojo.Listviewclass;

import java.util.ArrayList;


/**
 * Created by Razil on 18-Jun-18.
 */
public class MyBaseAdapter extends android.widget.BaseAdapter implements Filterable {
    public static ArrayList<Listviewclass> frdlist = new ArrayList();//originale
    public static ArrayList<Listviewclass> frd2 = new ArrayList<>();//to display value
    private Context context;
    private LayoutInflater linear ;
private  static  final String TAG="MyBaseAdapter";

    public MyBaseAdapter(Context context1, ArrayList<Listviewclass> array) {
        this.frdlist = array;
        this.frd2 = array;
        this.context = context1;
    }

    @Override
    public int getCount() {
        return frd2.size();
    }

    @Override
    public Object getItem(int i) {
        return frd2.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View converterview, ViewGroup viewGroup) {
        final MyViewHolder holder;
        if (converterview == null) {
            holder = new MyViewHolder();
            converterview = LayoutInflater.from(context).inflate(R.layout.activity_listview_item, null, false);
            holder.chk = (CheckBox) converterview.findViewById(R.id.chklist);
            converterview.setTag(holder);
        } else {
            holder = (MyViewHolder) converterview.getTag();

        }
        holder.chk.setText(frd2.get(position).fname);
        holder.chk.setChecked(frd2.get(position).getSelected());
        holder.chk.setTag(R.integer.flag, converterview);
        converterview.setTag(holder);

        holder.chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"positiob "+position);
                if (frd2.get(position).getSelected()) {
                    frd2.get(position).setSelected(false);
                } else {
                    frd2.get(position).setSelected(true);
                }

            }
        });


        return converterview;
    }

    private class MyViewHolder {

        protected CheckBox chk;
    }

    @Override
    public Filter getFilter() {
        Log.i("MyBaseAdapter", "filter function call");
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                frd2 = (ArrayList<Listviewclass>) filterResults.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }


            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();   // Holds the results of a filtering operation in values
                ArrayList<Listviewclass> FilteredArrList = new ArrayList<Listviewclass>();
                if (frdlist == null) {
                    frdlist = new ArrayList<Listviewclass>(frd2);
                }
                if (charSequence == null || charSequence.length() == 0) {
                    results.count = frdlist.size();
                    results.values = frdlist;
                } else {
                    charSequence = charSequence.toString().toLowerCase();
                    for (int i = 0; i < frdlist.size(); i++) {
                        String data = frdlist.get(i).fname;
                        if (data.toLowerCase().contains(charSequence.toString())) {
                            FilteredArrList.add(new Listviewclass(frdlist.get(i).fname));

                        }
                    }
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }

        };
        return filter;

    }
}
