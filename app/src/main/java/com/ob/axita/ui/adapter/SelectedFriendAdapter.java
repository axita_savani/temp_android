package com.ob.axita.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.core.pojo.User;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Razil on 23-Jul-18.
 */
public class SelectedFriendAdapter extends RecyclerView.Adapter<SelectedFriendAdapter.ViewHolder> {

    private static final String TAG = "SelectedFriendAdapter";
    Context context;
    private List<User>FriendnList;
    List<Long> array;

    public SelectedFriendAdapter(Context context, List<User> friendlist) {
        this.context = context;
this.FriendnList=friendlist;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_friend_whole, parent, false);
        array=new ArrayList<>();
        return new ViewHolder(v1);

    }

    @Override
    public void onBindViewHolder(@NonNull SelectedFriendAdapter.ViewHolder holder, int position) {
        final User user = FriendnList.get(position);
        holder.name.setText(user.getName());
       // Log.i(TAG,"friend list-->"+user.getName());
      //  Log.i(TAG,"Friend list -->"+user.getId());
       /* array.add(user.getId());
        if(FriendnList!=null)
        {

        }
        for(Long array1:array)
        {
            Log.i(TAG,"ArrayFriend"+array1.toString());
        }*/
    }

    @Override
    public int getItemCount() {
        return FriendnList.size();
    }
    @Override
    public long getItemId(int position) {
        return FriendnList.get(position).getId();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(View view) {
            super(view);
            name=itemView.findViewById(R.id.FriendNameSelected);

        }
    }
}
