package com.ob.axita.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ob.axita.R;
import com.ob.axita.core.constants.Extras;
import com.ob.axita.core.database.dao.FriendDAO;
import com.ob.axita.core.database.dao.UserDAO;
import com.ob.axita.core.pojo.Friend;
import com.ob.axita.core.pojo.User;
import com.ob.axita.ui.activity.BaseFragmentActivity;
import com.ob.axita.ui.activity.MainActivity;
import com.ob.axita.ui.activity.MainActivityNew;
import com.ob.axita.ui.adapter.UserAdapter;
import com.ob.axita.ui.interfaces.HostInterface;
import com.ob.axita.ui.interfaces.RecyclerViewItemInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Razil on 19-Jul-18.
 */
public class SqlDatabaseFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "SqlDatabaseFragment";
    private Context context;
    private HostInterface hostInterface;
    private Button adduser;

    private RecyclerView userRecylerView;
    private UserAdapter userAdapter;

    UserDAO userDAO;
    User user;



    @Override
    public void onAttach(Context activity) {
        this.context = activity;
        hostInterface = (HostInterface) context;
        userDAO = new UserDAO(context);
        super.onAttach(activity);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("SQL Database");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sql_database, container, false);
        this.adduser = view.findViewById(R.id.btnAddUser);
        this.userRecylerView = view.findViewById(R.id.userDisplayRecylerView);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        userRecylerView.setLayoutManager(llm);

        List<User> userList = userDAO.getAllUser();
        for (User user1 : userList) {
            String log = "Id: " + user1.getId() + " ,Name: " + user1.getName() + " ,Email: " + user1.getEmail();
            Log.i(TAG, " Userdata-->" + log);
        }
        userAdapter = new UserAdapter(getContext(), userDAO.getAllUser());
        Log.i(TAG, "Adapter added");
        userRecylerView.setAdapter(userAdapter);

        adduser.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        userAdapter = new UserAdapter(getContext(), userDAO.getAllUser());
        userRecylerView.setAdapter(userAdapter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnAddUser:
                Fragment_Add_User fragmentAddUser = new Fragment_Add_User();
                Intent intent = new Intent(context, BaseFragmentActivity.class);

                intent.putExtra(Extras.EXTRA_FRAGMENT_SIGNUP, fragmentAddUser);
                context.startActivity(intent);

        }
    }




}
