package com.ob.axita.ui.fragment;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;


public class DrawblesFragment extends Fragment {
    private AnimationDrawable animationDrawable;
    private ImageView imageView;
    private TextView txtview;
    private EditText editText;
    private HostInterface hostInterface;
    Context context;


    public static Fragment newInstance() {
        DrawblesFragment fragment = new DrawblesFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        hostInterface= (HostInterface) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context=getContext();
        hostInterface.setToolbarTitle("Drawbles");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragement_drawbles, container, false);
        this.imageView = view.findViewById(R.id.imageanimation);

        this.txtview = view.findViewById(R.id.textview_demo);
        this.editText = view.findViewById(R.id.edittext_demo);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtview.setText(editText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
