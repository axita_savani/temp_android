package com.ob.axita.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ob.axita.R;
import com.ob.axita.ui.adapter.NumberRecyclerAdapter;
import com.ob.axita.ui.interfaces.HostInterface;

/**
 * Created by Razil on 25-Jun-18.
 */
public class RecyclerViewDisplayFragment extends Fragment  {
    private static final String TAG = "RecyclerViewFragment";

   private RecyclerView recyclerView;
    private NumberRecyclerAdapter numberRecyclerAdapter;
    private int cloum = 5;
   private Context context;
    private HostInterface hostInterface;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hostInterface= (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getContext();
        hostInterface.setToolbarTitle("Recyler(grid view)");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_display_list_select, container, false);

        recyclerView = view.findViewById(R.id.recyleview_display_gridview);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

            String[] no = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "3  7", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"};

           this.recyclerView.setLayoutManager(new GridLayoutManager(context,3));
            numberRecyclerAdapter = new NumberRecyclerAdapter(super.getContext(), no);
            recyclerView.setAdapter(numberRecyclerAdapter);
            Log.i(TAG, "nullpointer----->>>"+"    "+super.getContext());


    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
