package com.ob.axita.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.ob.axita.R;
import com.ob.axita.core.Utils.FileUtils;
import com.ob.axita.ui.interfaces.HostInterface;

import java.io.File;
import java.util.List;
import java.util.Objects;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/** Created by Razil on 25/07/2018. */
public class CamerePickerFragment extends Fragment
        implements View.OnClickListener, EasyPermissions.PermissionCallbacks {

    static final String TAG = "CamerePickerFragment";
    static final int REQUEST_CODE_ASK_STORAGE_PERMISSIONS = 123;
    ImagePickerCallback imagePickerCallback;

    Context context;
    private HostInterface hostInterface;

    private Button btnCamera, btnGallery;
    private CameraImagePicker cameraImagePicker;

    private String CameraPickerpath;
    private String GalleryPickerPath;
    private ImageView imgCamera, imgGallery;
    private ImagePicker imagePicker;
    private int SELECT_OPTION = 1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        hostInterface = (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Image Picker");
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_picker, container, false);
        this.btnCamera = view.findViewById(R.id.btnImageCamera);
        this.btnGallery = view.findViewById(R.id.btnImageGallery);
        this.imgCamera = view.findViewById(R.id.imgcamerapicker);
        this.imgGallery = view.findViewById(R.id.imggallerypicker);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imagePickerCallback = new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> list) {
                try {
                    Log.i(TAG, "onImagesChosen()" + list.size());
                            if (list.size() == 0) {
                                Snackbar.make(btnGallery, "No Image Pick", Snackbar.LENGTH_LONG)
                                        .show();
                                return;
                            }
                            final ChosenImage chosenImage = list.get(0);
                            Activity activity = getActivity();
                            if (activity != null && isAdded()) {
                                getActivity()
                                        .runOnUiThread(new Runnable() {
                                            @Override
                                                    public void run() {
                                                        if (chosenImage != null) {
                                                            Log.i(TAG, "getOriginalPath()" + chosenImage.getOriginalPath());
                                                            Log.i(TAG, "getOrientationName()" + chosenImage.getOrientationName());
                                                            Log.i(TAG, "getExtension()" + chosenImage.getExtension());
                                                            Log.i(TAG, "getFileExtensionFromMimeType()" + chosenImage.getFileExtensionFromMimeType());
                                                            FileDataSet(chosenImage.getOriginalPath());

                                                        }
                                                    }
                                                });
                            }

                        } catch (Throwable th) {
                                th.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(String s) {
                        Log.i(TAG, "onError()");
                    }
                };
        btnGallery.setOnClickListener(this);
        btnCamera.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnImageCamera:
                SELECT_OPTION=1;
                checkForStoragePermissions();
                takepicker();
                break;
            case R.id.btnImageGallery:
                SELECT_OPTION = 2;
                checkForStoragePermissions();
                takeGallery();
                break;
        }
    }

    private void takeGallery() {
        imagePicker = new ImagePicker(this);
        imagePicker.setImagePickerCallback(imagePickerCallback);
        imagePicker.shouldGenerateMetadata(false);
        imagePicker.shouldGenerateThumbnails(false);
        imagePicker.pickImage();
    }

    private void takepicker() {

        cameraImagePicker = new CameraImagePicker(this);
        cameraImagePicker.setDebugglable(true);
        cameraImagePicker.setImagePickerCallback(imagePickerCallback);
        cameraImagePicker.shouldGenerateMetadata(false); // default it is true
        cameraImagePicker.shouldGenerateThumbnails(false); // default it is true
        CameraPickerpath = cameraImagePicker.pickImage();
        Log.i(TAG, "takepicker" + CameraPickerpath);
    }
    private void FileDataSet(String path) {
        if(SELECT_OPTION==1)
        {
            if (path != null) {
                File file = new File(path);
                long length = file.length();
                Log.i(TAG, "File Length" + length);
                CameraPickerpath = path;
                Log.i(TAG, "CameraPickerpath: " + CameraPickerpath);
                setImageCamera(com.ob.axita.core.Utils.FileUtils.validateFileUrl(CameraPickerpath,1));
            }
        }
        else if(SELECT_OPTION==2)
        {
            if (path != null) {
                File file = new File(path);
                long length = file.length();
                Log.i(TAG, "File Length" + length);
                Log.i(TAG, "GalleryPickerPath: " + path);
                setImageGallery(FileUtils.validateFileUrl(path,2));
            }
        }

    }

    private void setImageGallery(String path) {
        if(path!=null)
        {
            Log.i(TAG,"PATH:-->"+path);
            Glide.with(getActivity().getApplicationContext()).load(path).into(imgGallery);
        }

    }

    private void setImageCamera(String FilePath) {
        if (FilePath == null || FilePath.length() == 0) {
                Log.e(TAG, "No Image Camera");
                imgCamera.setImageResource(0);
                return;
            }
            Glide.with(getActivity().getApplicationContext()).load(FilePath).into(imgCamera);
        }


    //////////////////// Runtime Permission Get USing Easy permission////////////////
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @AfterPermissionGranted(REQUEST_CODE_ASK_STORAGE_PERMISSIONS)
    private void checkForStoragePermissions() {
        Log.i(TAG, "checkForStoragePermissions()");
        if (EasyPermissions.hasPermissions(
                Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // If permission is already granted
            Log.i(TAG, "PERMISSION GRANTED");

        } else {
            Log.i(TAG, "REQUEST_PERMISSION_CAMERA");
            EasyPermissions.requestPermissions(
                    this,
                    " TestAndroid would like to use your camera",
                    REQUEST_CODE_ASK_STORAGE_PERMISSIONS,
                    Manifest.permission.CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> list) {
        // Do nothing.
        Log.i(TAG, "onPermissionsGranted(): " + list);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> list) {
        Log.i(TAG, "onPermissionsDenied() -> " + list);
        Snackbar.make(
                        btnGallery,
                        "Sorry, but that action is not permitted",
                        Snackbar.LENGTH_INDEFINITE)
                .setAction(
                        "Try Again",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(View v) {
                                checkForStoragePermissions();
                            }
                        })
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "Request code" + requestCode);
        Log.i(TAG, "Data -->" + data);
        switch (requestCode) {
            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (cameraImagePicker == null) {
                        cameraImagePicker = new CameraImagePicker(getActivity());
                        cameraImagePicker.reinitialize(CameraPickerpath);
                        cameraImagePicker.setImagePickerCallback(imagePickerCallback);
                    }
                    cameraImagePicker.submit(data);
                } else {
                    if (CameraPickerpath != null && CameraPickerpath.length() > 0) {

                        FileDataSet(CameraPickerpath);
                        Log.i(TAG, "FileDataSet()-->onActivityResult()");
                    } else {
                        Snackbar.make(btnGallery, "Failed to pick image", Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(getActivity());
                        imagePicker.setImagePickerCallback(imagePickerCallback);
                    }
                    imagePicker.submit(data);
                } else {
                    Log.i(TAG, " PICK_IMAGE_DEVICE:Result Code For " + resultCode);
                }
        }
    }
}
