package com.ob.axita.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ob.axita.R;

import java.util.List;


/**
 * Created by Razil on 20-Jun-18.
 */
public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    private static final String TAG = "FavoriteAdapter";
    private List<String> mdata;
    private List<String> mno;
    private LayoutInflater layoutInflater;
    private List<String> images;
    Context context;
    int radius = 30; // corner radius, higher value = more rounded
    int margin = 10; // crop margin, set to 0 for corners with no crop

    public FavoriteAdapter(Context context, List<String> data, List<String> data1, List<String> data2) {
        this.layoutInflater = LayoutInflater.from(context);
        this.mdata = data;
        this.mno = data1;
        this.images = data2;
        this.context = context;
    }

    @NonNull
    @Override
    public FavoriteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @SuppressWarnings("Annotator") View view = layoutInflater.inflate(R.layout.text_view_recycler, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.i(TAG,"path="+images.get(position));
        String name = mdata.get(position);
        String no = mno.get(position);
        String str= "file:///android_asset/images/";
        String image=str.concat(images.get(position));
        Log.i(TAG,"s="+image);
        holder.textView1.setText(name);
        holder.textView2.setText(no);
        Glide
                .with(context).load(Uri.parse(image))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Log.i(TAG, "Image loading fail" + e);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Log.i(TAG, "Loading image completed");
                        return false;
                    }
                }).into(holder.imageView);


    }

    //total no of rows
    @Override
    public int getItemCount() {
        return mdata.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView1;
        TextView textView2;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.glide_image);
            textView1 = itemView.findViewById(R.id.txtname);
            textView2 = itemView.findViewById(R.id.txtno);


        }
    }

}
