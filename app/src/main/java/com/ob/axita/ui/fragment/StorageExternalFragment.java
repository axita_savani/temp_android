package com.ob.axita.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;


public class StorageExternalFragment extends Fragment implements View.OnClickListener {
    Context context;
    private HostInterface hostInterface;
    private EditText editfiledata;
    private Button btnpublic,btnprivate,btndisplay;
    private TextView txtdata;

    @Override
    public void onAttach(Context c) {
        this.context = c;
        hostInterface = (HostInterface) context;
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("External Storage");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_storage_external, container, false);
        this.editfiledata=view.findViewById(R.id.edit_file_data);
        this.btnpublic=view.findViewById(R.id.btn_save_public);
        this.btnprivate=view.findViewById(R.id.btn_save_private);
        this.txtdata=view.findViewById(R.id.txtdata);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_save_public:
                break;
            case R.id.btn_save_private:
                break;
        }
    }
}
