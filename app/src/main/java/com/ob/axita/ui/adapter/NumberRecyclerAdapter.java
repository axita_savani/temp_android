package com.ob.axita.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ob.axita.R;

/**
 * Created by Razil on 25-Jun-18.
 */
public class NumberRecyclerAdapter extends RecyclerView.Adapter<NumberRecyclerAdapter.ViewHolder> {
    private Context context;
    private String[] mdata;

    public NumberRecyclerAdapter(Context context1, String[] data) {
        this.context = context1;
        this.mdata = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.number_grid_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(mdata[position]);
    }

    @Override
    public int getItemCount() {
        return mdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textview4);
        }
    }
}
