package com.ob.axita.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;


public class EnterFriendListviewFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "EnterFriendListview";
    Context context;
    HostInterface hostInterface;
    EditText txtadd;
    Button btnadd;

    ListViewSelectFragment listViewSelectFragment;

    @Override
    public void onAttach(Context con) {
        this.context = con;
        hostInterface = (HostInterface) context;
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Friend Add");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enter_friend_listview, container, false);
        this.txtadd = view.findViewById(R.id.txt_add_friend);
        this.btnadd = view.findViewById(R.id.btn_add_friend);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        listViewSelectFragment = new ListViewSelectFragment();
        super.onViewCreated(view, savedInstanceState);
        ;

        btnadd.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_friend:
                Log.i(TAG, "Add Click");
                //FragmentManager fragmentManager=getFragmentManager();
                //FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //fragmentTransaction.replace(R.id.layoutFHostFragment, listViewSelectFragment);
                Intent intent = new Intent(getActivity(), EnterFriendListviewFragment.class);
                intent.putExtra("Friend", txtadd.getText().toString());
                assert getTargetFragment() != null;
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                //fragmentTransaction.commit();

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layoutFHostFragment, listViewSelectFragment).commit();
                break;
        }
    }

}
