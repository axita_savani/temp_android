package com.ob.axita.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.ui.adapter.MyBaseAdapter;
import com.ob.axita.ui.interfaces.HostInterface;




/**
 * Created by Razil on 25-Jun-18.
 */
public class DisplayListFragment extends Fragment {
    private TextView textView;
    private HostInterface hostInterface;
    Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hostInterface = (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context = getContext();
        hostInterface.setToolbarTitle("Selected Friend");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentselecteddisplay, container, false);
        this.textView = view.findViewById(R.id.display_friend);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        for (int i = 0; i < MyBaseAdapter.frd2.size(); i++) {
            if (MyBaseAdapter.frd2.get(i).getSelected()) {
                Log.i("DisplayListFragment","--text--"+textView);
                textView.setText(textView.getText() + "\n\n" + MyBaseAdapter.frd2.get(i).getFname());
            }
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
