package com.ob.axita.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.core.database.dao.FriendDAO;
import com.ob.axita.core.pojo.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Razil on 21-Jul-18.
 */
public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    private static final String TAG = "FriendAdapter";
    private Context context;
    private List<User> friendList;
    FriendDAO friendDAO;
    private long friendID;


    public FriendAdapter(Context context, List<User> data, long friendid) {
        this.context = context;
        this.friendList = data;
        this.friendID = friendid;
        friendDAO = new FriendDAO(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_display_friend, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final User user = friendList.get(position);
        holder.name.setText(user.getName());
        Log.i(TAG, "item added" + position);
        Log.i(TAG, "USER NAME-->" + user.getName());
        Log.i(TAG,"USER ID-->"+user.getId());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (friendList != null) {

                    friendDAO.insertFriendData(user.getId(), friendID,user.getName());
                    friendList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    Log.i(TAG, "list ite remove" + position);
                    Log.i(TAG, "friend click" + user.getId() + "friend id" + friendID);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }

    @Override
    public long getItemId(int position) {
        return friendList.get(position).getId();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView addfriend;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.FriendName);
            addfriend = itemView.findViewById(R.id.ButtonAddFriend);

        }
    }


}

