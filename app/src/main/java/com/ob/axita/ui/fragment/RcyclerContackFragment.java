package com.ob.axita.ui.fragment;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ob.axita.R;
import com.ob.axita.ui.adapter.FavoriteAdapter;
import com.ob.axita.ui.interfaces.HostInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class RcyclerContackFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "RcyclerContackFragment";
    private RecyclerView mRecyclerView;
    FavoriteAdapter adapter;
    private HostInterface hostInterface;
    Context context;
    AssetManager assetManager;
    List<String> imagelist;



    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        hostInterface = (HostInterface) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getContext();
        hostInterface.setToolbarTitle("Recyler(Contact)");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclercontack, container, false);
        this.mRecyclerView = view.findViewById(R.id.recyleview);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<String> name = new ArrayList<>();

        name.add("axita");
        name.add("meera");
        name.add("mitul");
        name.add("jamnesh");

        name.add("jessica");
        name.add("axita");
        name.add("meera");
        name.add("mitul");

        name.add("jamnesh");
        name.add("jessica");
        name.add("axita");
        name.add("meera");

        name.add("mitul");
        name.add("jamnesh");
        name.add("jessica");


        ArrayList<String> no = new ArrayList<>();

        no.add("9586065190");
        no.add("7227006024");
        no.add("9925718246");
        no.add("8530280258");

        no.add("8758359321");
        no.add("9586065190");
        no.add("7227006024");
        no.add("9925718246");

        no.add("8530280258");
        no.add("8758359321");
        no.add("9586065190");
        no.add("7227006024");

        no.add("9925718246");
        no.add("8530280258");
        no.add("8758359321");
        try {
            AssetManager assetManager = context.getAssets();
            String[] files = assetManager.list("images");

            Log.i(TAG,"assets image catched");
            imagelist = Arrays.asList(files);

        }
         catch (IOException e) {
            e.printStackTrace();
        }
        /*AssetManager assetManager = getAssets();
        List<String> images=assetManager.list("images");
ArrayList<String> listimage=new ArrayList<String>(images);
*/
       /* image.add("http://i.imgur.com/rFLNqWI.jpg");
        image.add("http://i.imgur.com/C9pBVt7.jpg");
        image.add("http://i.imgur.com/rT5vXE1.jpg");
        image.add("http://i.imgur.com/aIy5R2k.jpg");

        image.add("http://i.imgur.com/rFLNqWI.jpg");
        image.add("http://i.imgur.com/C9pBVt7.jpg");
        image.add("http://i.imgur.com/rT5vXE1.jpg");
        image.add("http://i.imgur.com/aIy5R2k.jpg");

        image.add("http://i.imgur.com/rFLNqWI.jpg");
        image.add("http://i.imgur.com/C9pBVt7.jpg");
        image.add("http://i.imgur.com/rT5vXE1.jpg");
        image.add("http://i.imgur.com/aIy5R2k.jpg");
        image.add("http://i.imgur.com/rFLNqWI.jpg");
        image.add("http://i.imgur.com/C9pBVt7.jpg");
        image.add("http://i.imgur.com/rT5vXE1.jpg");
       */ /* ("file:///E://axita//image_glide//ALIABHATT.png");*/


        adapter = new FavoriteAdapter(getContext(), name, no,imagelist);

        //lauout define
        //recycle view set up

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        Log.i(TAG, "-->" + getContext());

        mRecyclerView.setAdapter(adapter);


        //adapter added


    }



    @Override
    public void onClick(View view) {

    }
}
