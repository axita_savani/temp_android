package com.ob.axita.ui.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;

import java.util.ArrayList;


public class DialogMultiChoiceFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "DialogMultiChoice";
    Context context;
    private HostInterface hostInterface;
    private Button btnmultidialog;
    private TextView txtdialog;


    private String[] multichoice = {"Baghhi 2", "Race 3", "Gold", "Dhadk", "Avengers"};
    private final boolean[] check = {false, false, false, false, false, false};
    private ArrayList<String> selected = new ArrayList<>();

    @Override
    public void onAttach(Context con) {
        this.context = con;
        super.onAttach(context);
        hostInterface = (HostInterface) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Multi Choice Dialog");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_dialog_multi_choice, container, false);
        this.btnmultidialog = view.findViewById(R.id.btn_dialog_multi);
        this.txtdialog = view.findViewById(R.id.txtdialog);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnmultidialog.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        final AlertDialog.Builder alertmulti = new AlertDialog.Builder(context);
        alertmulti.setTitle("Select Your Favorite Movie");
        alertmulti.setMultiChoiceItems(multichoice, check, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                if (b) {
                    selected.add(multichoice[i]);
                    Log.i(TAG, "click" + selected);

                } else if (selected.contains(multichoice[i])) {
                    selected.remove(multichoice[i]);
                }


            }
        });
        alertmulti.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertmulti.setCancelable(true);
                txtdialog.setText("  " + selected);

            }
        });
        alertmulti.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(context, "You not select Movie", Toast.LENGTH_LONG).show();
            }
        });
        alertmulti.show();
    }
}
