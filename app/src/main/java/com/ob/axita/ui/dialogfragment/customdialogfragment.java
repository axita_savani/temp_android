package com.ob.axita.ui.dialogfragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ob.axita.R;
import com.ob.axita.ui.fragment.DialogCustomInputFragment;

/**
 * Created by Razil on 29-Jun-18.
 */
public class customdialogfragment extends DialogFragment {
    private static final String TAG = "customdialogfragment";

    Context context;


    @Override
    public void onAttach(Context con) {
        this.context = con;
        super.onAttach(context);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_custom_layout, new LinearLayout(getActivity()), false);
        final EditText name = view.findViewById(R.id.dialog_edit_name);
        final EditText psd = view.findViewById(R.id.dialog_edit_password);
        final EditText conpsd = view.findViewById(R.id.dialog_edit_confirm);
        Log.i(TAG, "id name" + name.getId());
        Log.i(TAG, "Conext" + context);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setTitle("Input Data");
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(context, DialogCustomInputFragment.class);
                Log.i(TAG, "name" + name.getText().toString());
                Log.i(TAG, "psd:" + psd.getText().toString());
                Log.i(TAG, "con psd:" + conpsd.getText().toString());

                if (name.getText().toString().trim().length() != 0 || psd.getText().toString().trim().length() != 0 || conpsd.getText().toString().trim().length() != 0) {
                    if (psd.getText().toString().trim().length() >= 6) {
                        if (psd.getText().toString().equals(conpsd.getText().toString())) {
                            intent.putExtra("Name", name.getText().toString());
                            intent.putExtra("password", psd.getText().toString());
                            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                            dismiss();
                        } else {

                            Toast.makeText(getActivity(), "Password Not Match!! try again", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(getActivity(), "Password More Then 6 Characte !! try again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Fields Not Empty !! try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getTargetFragment().onActivityResult(getTargetRequestCode(),Activity.RESULT_CANCELED,null);
                dismiss();
            }
        });
        return builder.create();
    }


}
