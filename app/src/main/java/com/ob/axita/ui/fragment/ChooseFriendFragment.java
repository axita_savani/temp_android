package com.ob.axita.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ob.axita.R;
import com.ob.axita.core.constants.Extras;
import com.ob.axita.core.database.dao.FriendDAO;
import com.ob.axita.core.database.dao.UserDAO;
import com.ob.axita.core.pojo.User;
import com.ob.axita.ui.adapter.FriendAdapter;

import java.util.List;

/** Created by Razil on 21-Jul-18. */
public class ChooseFriendFragment extends BaseFragment {
    private static final String TAG = "ChooseFriendFragment";

    private RecyclerView friendRecylerView;
    UserDAO userDAO;
    FriendDAO friendDAO;
    User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Friend List");
        userDAO = new UserDAO(getActivity());
        friendDAO = new FriendDAO(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {

            user = (User) bundle.getSerializable(Extras.EXTRA_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_user_friend, container, false);
        this.friendRecylerView = view.findViewById(R.id.recylerfriendSelect);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        friendRecylerView.setLayoutManager(llm);

        if (user != null) {
            setToolbarTitle(user.getName());
            Log.i(TAG, "get bundle-->" + user.getId());
            ////////////////////////////// friend list///////////////

           /* List<User> list = friendDAO.getAddfriednList(user.getId());
            Log.i(TAG,"friend function call");
            for (User user2 : list) {
                String log = "Id: " + user2.getId() ;
                Log.i(TAG, " Userdata which added into friend list -->" + log);
            }*/
            //////////////// fet all user except login/////////////////////
            List<User> userList = userDAO.getFriendList(user.getId());
            for (User user1 : userList) {
                String log = "Id: " + user1.getId() + " ,Name: " + user1.getName() + " ,Email: ";
                Log.i(TAG, "getFriendList()-->" + log);
            }

            FriendAdapter friendAdapter = new FriendAdapter(getActivity(), userList, user.getId());
            Log.i(TAG, "Adapter added Friend");
            friendRecylerView.setAdapter(friendAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public BaseFragment createFragment(Bundle bundle) {
        return null;
    }


}
