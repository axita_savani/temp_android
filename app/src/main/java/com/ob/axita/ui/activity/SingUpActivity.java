package com.ob.axita.ui.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ob.axita.R;


public class SingUpActivity extends AppCompatActivity {
    private static final String TAG = "SingUpActivity";
    TextView textView1;
    android.support.v7.widget.Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        toolbar = findViewById(R.id.toolbar1);

        Log.i(TAG, "toolbar id get");
       // setSupportActionBar(toolbar);
        toolbar.setTitle("Sign Up");
        toolbar.setNavigationIcon(R.drawable.back38);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.i(TAG, "back to main activity");
                finish();
            }
        });
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://www.optimumbrew.com"));
                startActivity(browserIntent);
                Log.i(TAG, "SingUpActivity link 1");

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://www.optimumbrew.com"));
                startActivity(browserIntent);
                Log.i(TAG, "SingUpActivity link 2");

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        SpannableStringBuilder str = new SpannableStringBuilder("By signing up you agree to our TERMS OF USE and ");
        SpannableStringBuilder str2 = new SpannableStringBuilder("PRIVACY POLICY");

        str.setSpan(clickableSpan, 31, 43, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new StyleSpan(Typeface.BOLD), 31, 43, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        str2.setSpan(clickableSpan1, 0, str2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        str2.setSpan(new StyleSpan(Typeface.BOLD), 0, str2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView1 = findViewById(R.id.link);
        textView1.setText(str);
        textView1.setText(str.append(str2));
        textView1.setMovementMethod(LinkMovementMethod.getInstance());


    }


}
