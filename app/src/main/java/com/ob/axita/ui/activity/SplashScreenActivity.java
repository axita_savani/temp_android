package com.ob.axita.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.ob.axita.R;

import static com.ob.axita.core.constants.Constants.SPLASH_SCREEN_TIME;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //TODO GO to main screen
                Intent intent = new Intent(SplashScreenActivity.this, MainActivityNew.class);
                startActivity(intent);
                finish();

            }
        }, SPLASH_SCREEN_TIME);

    }
}
