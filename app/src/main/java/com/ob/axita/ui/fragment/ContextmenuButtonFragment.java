package com.ob.axita.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;

/**
 * Created by Razil on 28-Jun-18.
 */
public class ContextmenuButtonFragment extends Fragment {
    Context context;
    private Button btnmenu;
    private HostInterface hostInterface;
    @Override
    public void onAttach(Context activity) {
        this.context = activity;
        super.onAttach(context);
        hostInterface= (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Context Menu Button");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_contextmenu_button,container,false);
        this.btnmenu=view.findViewById(R.id.btn);
        registerForContextMenu(btnmenu);
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater =getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_contextmenu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo contextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menufriend:
                Toast.makeText(context, "Friend option selected", Toast.LENGTH_LONG).show();
                break;
            case R.id.menufamily:
                Toast.makeText(context, "Family option selected", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuPeople:
                Toast.makeText(context, "people option selected", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuAbout:
                Toast.makeText(context, "About option selected", Toast.LENGTH_LONG).show();
                break;

        }
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
