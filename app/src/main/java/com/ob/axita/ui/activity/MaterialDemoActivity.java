package com.ob.axita.ui.activity;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.ui.adapter.ViewPagerAdapter;
import com.ob.axita.ui.fragment.ListViewContactFragment;
import com.ob.axita.ui.interfaces.HostInterface;

public class MaterialDemoActivity extends AppCompatActivity implements HostInterface {
    private static final String TAG = "MaterialDemoActivity";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Button btnfirst, btnsecond,btndemo;
    private FloatingActionButton fab, fab1, fab2, fab3;
    private boolean isFABOpen = false;
    private BottomNavigationView bottomNavigationView;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;


    private TransitionDrawable transitionDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_materialdemo);
        initialization();


        toolbar.setTitle("Design Material");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        transitionDrawable = (TransitionDrawable) fab.getDrawable();
        transitionDrawable.setCrossFadeEnabled(true);



//navigation code
        //set navigation listener into navigation
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //do any particular item is selected default
                Log.i(TAG, "navigation button click");
                item.setChecked(true);
                //close drawer when item is selected
                drawerLayout.closeDrawers();
                return true;
            }
        });
      ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
           tabLayout.setupWithViewPager(viewPager);




       /* btnfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //passed fragment object create new one
                loadfragment(new Demo1Fragment());
            }
        });
        btnsecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadfragment(new Demo2Fragment());
            }
        });
       */
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    showFABmenu();
                   /* Snackbar snackbar = Snackbar.make(view, "you click on FAb", Snackbar.LENGTH_LONG).setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Snackbar snackbar1 = Snackbar.make(view, "message retrived", Snackbar.LENGTH_SHORT);
                            snackbar1.show();
                            closeFAbmenu();
                        }
                    });
                    snackbar.show();
*/

                } else {
                    closeFAbmenu();
                }
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                /*android.support.v4.app.Fragment selectedFragment = ListViewContactFragment.newInstance();
                switch (item.getItemId()) {
                    case R.id.first_home:
                        selectedFragment = ListViewContactFragment.newInstance();
                        break;
                    case R.id.second_like:
                        selectedFragment = DrawblesFragment.newInstance();
                        break;
                    case R.id.third_search:
                        selectedFragment = RcyclerContackFragment.newInstance();
                        break;
                }
                android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                final android.support.v4.app.FragmentTransaction replace = transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
*/
                return true;
            }
        });
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        //transaction.replace(R.id.frame_layout,ListViewContactFragment.newInstance());
        transaction.commit();


    }

    private void initialization() {
        //        btnfirst = (Button) findViewById(R.id.btn_first);
        //        btnsecond = (Button) findViewById(R.id.btn_second);
        /*toolbar = (Toolbar) findViewById(R.id.toolbar_design);
      *//*  fab = findViewById(R.id.fab_materialdemo);
        fab1 = findViewById(R.id.fab_fab1);
        fab2 = findViewById(R.id.fab_fab2);
        fab3 = findViewById(R.id.fab_fab3);
      *//*  drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_demo);

       // btndemo=findViewById(R.id.btn_demo);
          viewPager = findViewById(R.id.viewpager_demo);
        tabLayout = findViewById(R.id.tablayout_demo);
        */// bottomNavigationView = findViewById(R.id.navigation_bottom);


    }

    private void closeFAbmenu() {
        isFABOpen = false;
        transitionDrawable.resetTransition();
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
        fab3.animate().translationY(0);

    }

    private void showFABmenu() {
        transitionDrawable.startTransition(50);
        isFABOpen = true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        fab3.animate().translationY(-getResources().getDimension(R.dimen.standard_155));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i(TAG, "click on toolbar icon");

                drawerLayout.openDrawer(GravityCompat.START);
                closeFAbmenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void enableSearchInToolbar(boolean status) {

    }

    @Override
    public void setSearchHint(int hindId) {

    }

    @Override
    public void refreshNavigationDrawer() {

    }

    @Override
    public void changeCurrentFragmentTo(int currentFragmentId, Bundle bundle) {
        switch (currentFragmentId) {
            case 0:
                ListViewContactFragment srFragment = new ListViewContactFragment();
                ChangeCurrentFragment(srFragment);
                break;
        }
    }

    @Override
    public void setToolbarTitle(int resId) {

    }

    @Override
    public void changeDrawerItem(int drawerItemPosition) {

    }

    @Override
    public void setToolbarTitle(String format) {

    }

    @Override
    public void setTabSelection(int tabPosition) {

    }

    /**
     * Set current fragment
     */
    private void ChangeCurrentFragment(Fragment fragment) {
        //hideKeyBoard();
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        //fTransaction.setCustomAnimations(R.anim.fragment_enter_anim, R.anim.fragment_exit_anim);
      //  fTransaction.replace(R.id.layoutFHostFragment, fragment, fragment.getClass().getName());
        fTransaction.commitAllowingStateLoss();
    }

/*
    private void loadfragment(Fragment fragment) {
//create fragment manager
        FragmentManager fm = getFragmentManager();
        //create a fragment Transaction  to begin the transaction ans replace the fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.replace(R.id.linearlayout, fragment);
        fragmentTransaction.commit();
    }
*/


}
