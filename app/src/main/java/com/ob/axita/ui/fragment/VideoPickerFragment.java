package com.ob.axita.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraVideoPicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenVideo;
import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;

import java.io.File;
import java.util.List;

/**
 * Created by Razil on 27/07/2018.
 */
public class VideoPickerFragment extends Fragment implements View.OnClickListener,VideoPickerCallback {
    public  static  final  String TAG="VideoPickerFragment";
    Context context;
    HostInterface hostInterface;
 //   private ImageView imgvideoCamera,imgVideoGallery;
    private VideoView imgvideoCamera,imgVideoGallery;
    private Button btnVideoCamera,btnVideoGallery;
    private VideoPicker videoPicker;
    private CameraVideoPicker cameraVideoPicker;
    private  String CAMERA_PICKER_PATH=null;
    private String GALLERY_PICKER_PATH=null;
    private  int SELECT_OPTION=1;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
        hostInterface= (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Video Picker");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_video_picker,container,false);
        this.btnVideoCamera=view.findViewById(R.id.btnvideocamera);
        this.btnVideoGallery=view.findViewById(R.id.btnvideoGallery);
        this.imgvideoCamera=view.findViewById(R.id.imgVideoCamera);
        this.imgVideoGallery=view.findViewById(R.id.imgVideoGallery);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       btnVideoGallery.setOnClickListener(this);
       btnVideoCamera.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnvideocamera:
                SELECT_OPTION=1;
                CameravideoPicker();
                break;
            case R.id.btnvideoGallery:
                SELECT_OPTION=2;
                videopicker();
                break;
        }
    }

    private void CameravideoPicker() {
        cameraVideoPicker=new CameraVideoPicker(this);
        cameraVideoPicker.shouldGenerateMetadata(false);//true Default
        cameraVideoPicker.shouldGeneratePreviewImages(true);
        Bundle bundle=new Bundle();
        bundle.putInt(MediaStore.EXTRA_VIDEO_QUALITY,0);//Default is 1:High
        bundle.putInt(MediaStore.EXTRA_DURATION_LIMIT,15);//in Second
        bundle.putInt(MediaStore.EXTRA_SIZE_LIMIT,2048);
        Log.i(TAG,"BUNDLE"+bundle);
        cameraVideoPicker.setExtras(bundle);
        cameraVideoPicker.setVideoPickerCallback(this);
        CAMERA_PICKER_PATH=cameraVideoPicker.pickVideo();
        Log.i(TAG,"CAMERA_PICKER_PATH"+CAMERA_PICKER_PATH);


    }

    private void videopicker() {
        videoPicker=new VideoPicker(this);
        videoPicker.shouldGenerateMetadata(false);//defualt it is true
        videoPicker.shouldGeneratePreviewImages(false);//default true
        videoPicker.setVideoPickerCallback(this);
        videoPicker.pickVideo();

    }

    @Override
    public void onVideosChosen(List<ChosenVideo> list) {
        if(SELECT_OPTION==1)
        {
            MediaController mediaController=new MediaController(context);
            imgvideoCamera.setMediaController(mediaController);
           // mediaController.setAnchorView(imgvideoCamera);
            mediaController.show();
           // Glide.with(context).load(Uri.fromFile(new File(CAMERA_PICKER_PATH))).into();
            imgvideoCamera.setVideoURI(Uri.fromFile(new File(CAMERA_PICKER_PATH)));
            imgvideoCamera.start();
        }
        else if(SELECT_OPTION==2)
        {
            MediaController mediaController1=new MediaController(context);
            imgVideoGallery.setMediaController(mediaController1);
            mediaController1.show();
            final  ChosenVideo chosenVideo=list.get(0);
            Log.i(TAG,"getOriginalPath()"+chosenVideo.getOriginalPath());
            imgVideoGallery.setVideoPath(chosenVideo.getOriginalPath());
           // Glide.with(context).load(chosenVideo.getOriginalPath()).into(imgVideoGallery);
            imgVideoGallery.start();
        }

    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK && data!=null)
        {
            if(requestCode== Picker.PICK_VIDEO_DEVICE)
            {
                if (videoPicker == null) {
                    videoPicker = new VideoPicker(this);
                    videoPicker.setVideoPickerCallback(this);
                }
                videoPicker.submit(data);
                Log.i(TAG,"DATA-->GALLERY"+data);
            }
            else if(requestCode==Picker.PICK_VIDEO_CAMERA)
            {
                if(cameraVideoPicker==null)
                {
                    cameraVideoPicker=new CameraVideoPicker(this,CAMERA_PICKER_PATH);
                }
                cameraVideoPicker.submit(data);
                Log.i(TAG,"DATA-->CAMERA"+data);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
