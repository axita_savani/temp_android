package com.ob.axita.ui.activity;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Razil on 21-Jul-18.
 */
public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
