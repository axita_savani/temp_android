package com.ob.axita.ui.interfaces;

import android.os.Bundle;

/**
 * Created by Nirav Alagiya(nirav.alagiya@optimumbrew.com) on 02-Apr-15.
 */
public interface HostInterface {

    void enableSearchInToolbar(boolean status);

    void setSearchHint(int hindId);

    void refreshNavigationDrawer();

    void changeCurrentFragmentTo(int currentFragmentId, Bundle bundle);

    void setToolbarTitle(int resId);

    void changeDrawerItem(int drawerItemPosition);

    void setToolbarTitle(String format);

    void setTabSelection(int tabPosition);
}
