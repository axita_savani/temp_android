package com.ob.axita.ui.fragment;

import android.content.Context;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;


public class ContextmenuListviewFragment extends Fragment {
Context context;
    private ListView listViewmenu;
    private String[] menuitem = {"Abhishek", "Aditya", "Rahul", "Gaurav", "Saurabh", "Ankit", "Prateek", "Ashish", "Siddharth", "Shubham", "Rohit", "Nikhil", "Rishabh", "Mohit", "Akshay", "Amit", "Vaibhav", "Akash", "Yash", "Vivek", "Abhishek"};
    private ArrayAdapter<String> Array;
    private HostInterface hostInterface;
    @Override
    public void onAttach(Context con) {
        this.context=con;
        super.onAttach(context);
        hostInterface = (HostInterface) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        hostInterface.setToolbarTitle("Context Menu Listview");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_contextmenu_listview, container, false);
        this.listViewmenu = view.findViewById(R.id.listviewmenu);
        return  view;
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Array = new ArrayAdapter<String>(context, R.layout.layout_listmenu, R.id.listmenu, menuitem);
        listViewmenu.setAdapter(Array);
        registerForContextMenu(listViewmenu);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Action");

        menu.add(0, v.getId(), 0, "Friend");
        menu.add(0, v.getId(), 0, "Family");
        menu.add(0, v.getId(), 0, "Unknown");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="Friend")
        {
            Toast.makeText(context,"Good",Toast.LENGTH_SHORT).show();
        }
        else if(item.getTitle()=="Family")
        {
            Toast.makeText(context,"Very good",Toast.LENGTH_SHORT).show();
        }
        else if(item.getTitle()=="Unknown")
        {
            Toast.makeText(context,"Oops..",Toast.LENGTH_SHORT).show();
        }
        return true;    }


}
