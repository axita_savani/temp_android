package com.ob.axita.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.ob.axita.R;
import com.ob.axita.core.constants.Constants;
import com.ob.axita.core.database.DatabaseHelper;
import com.ob.axita.core.database.dao.FriendDAO;
import com.ob.axita.core.database.dao.UserDAO;
import com.ob.axita.core.pojo.User;
import com.ob.axita.ui.interfaces.HostInterface;

/**
 * Created by Razil on 19-Jul-18.
 */
public class Fragment_Add_User extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Fragment_Add_User";
    private Context context;

    private Button add, update, delete;
    private EditText username, useraddress, usergender, usereyecolor, userphone, useremail, userbalance, usercompany, userage, userabout;
    private SqlDatabaseFragment sqlDatabaseFragment;
    private DatabaseHelper databaseHelper;
    private User user;
    private boolean is_update = false;
    private UserDAO userDAO;
    private FriendDAO friendDAO;



    @Override
    public void onAttach(Context activity) {
        this.context = activity;

        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Add user");
        user = new User();
        databaseHelper = new DatabaseHelper(context);
        userDAO = new UserDAO(context);

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_user, container, false);
        this.add = view.findViewById(R.id.btnAdd);
        this.update = view.findViewById(R.id.btnUpdate);
        this.delete = view.findViewById(R.id.btnDelete);
        /////////////////////
        this.username = view.findViewById(R.id.userName);
        this.useraddress = view.findViewById(R.id.userAddress);
        this.userage = view.findViewById(R.id.userAge);
        this.userabout = view.findViewById(R.id.userabout);
        this.useremail = view.findViewById(R.id.userEmail);
        this.userbalance = view.findViewById(R.id.userBalance);
        this.usergender = view.findViewById(R.id.userGender);
        this.usereyecolor = view.findViewById(R.id.userEyecolor);
        this.userphone = view.findViewById(R.id.userPhone);
        this.usercompany = view.findViewById(R.id.userCompany);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sqlDatabaseFragment = new SqlDatabaseFragment();


        ///////////
        add.setOnClickListener(this);
        update.setOnClickListener(this);
        delete.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnAdd:
                addData();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().finish();
                    }
                }, Constants.SCREEN_CLOSE_TIME);
               // closeFragment();
                break;
            case R.id.btnUpdate:
                closeFragment();
                break;
            case R.id.btnDelete:
                closeFragment();
                break;
        }
    }

    private void addData() {
        Gson gson = new Gson();
        String jsonData = gson.toJson(getSelection(), User.class);
        Log.i(TAG, "data user" + jsonData);
        user.setId(userDAO.insertuser(user));

        }
    private User getSelection() {
        if (user == null) {
            user = new User();
        } else {
            user.setName(username.getText().toString());
            user.setAddress(useraddress.getText().toString());
            user.setAge(userage.getText().toString());
            user.setGender(usergender.getText().toString());
            user.setEmail(useremail.getText().toString());
            user.setPhone(userphone.getText().toString());
            user.setCompany(usercompany.getText().toString());
            user.setBalance(userbalance.getText().toString());
            user.setEyeColor(usereyecolor.getText().toString());
            user.setAbout(userabout.getText().toString());

        }
        return user;
    }

    private void closeFragment() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layoutFHostFragment, sqlDatabaseFragment).commit();
            }
        }, Constants.SCREEN_CLOSE_TIME);
    }
}
