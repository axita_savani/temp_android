package com.ob.axita.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.core.constants.Extras;
import com.ob.axita.core.pojo.User;
import com.ob.axita.ui.activity.BaseFragmentActivity;
import com.ob.axita.ui.fragment.DisplayWholeFragment;
import com.ob.axita.ui.fragment.SqlDatabaseFragment;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Razil on 20-Jul-18.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private static final String TAG = "UserAdapter";
    Context context;
    private List<User> userlist;
    SqlDatabaseFragment sqlDatabaseFragment;


    public UserAdapter(Context context, List<User> data) {
        this.context = context;
        this.userlist = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user, parent, false);
        return new ViewHolder(v1);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder,  int position) {
       final User user = userlist.get(position);
        holder.name.setText(user.getName());
        holder.email.setText(user.getEmail());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick" + holder.getAdapterPosition());

                Intent intent = new Intent(context, BaseFragmentActivity.class);
                DisplayWholeFragment displayWholeFragment = new DisplayWholeFragment();

                Bundle bundle = new Bundle();
                bundle.putSerializable(Extras.EXTRA_BRAND, user);

                intent.putExtra(Extras.EXTRA_FRAGMENT_BUNDLE, bundle);
                intent.putExtra(Extras.EXTRA_FRAGMENT_SIGNUP, displayWholeFragment);
                context.startActivity(intent);

                }
        });
    }

    @Override
    public int getItemCount() {
        return userlist.size();
    }

    @Override
    public long getItemId(int position) {
        return userlist.get(position).getId();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, email;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.Name);
            email = view.findViewById(R.id.Email);

        }
    }


}
