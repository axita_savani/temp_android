package com.ob.axita.ui.interfaces;

//////////////////////////////////////////////////////////////////////////////
//                   OptimumBrew Technology Pvt. Ltd.                       //
//                                                                          //
// Title:            YellowPage                                        //
// File:             com.optimumbrew.nepacall.ui.fragment                                        //
// Since:            04-Apr-2015 : PM 06:03           //
//                                                                          //
// Author:           Nirav Alagiya                                          //
// Email:            nirav.alagiya@optimumbrew.com                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

import android.os.Bundle;

import com.ob.axita.ui.fragment.BaseFragment;

import java.io.Serializable;

public interface FragmentProvider extends Serializable {

    /**
     * Extras key for fragment provider.
     */
    String EXTRA_FRAGMENT_PROVIDER = "com.optimumbrew.extra.FragmentProvider";
    /**
     * Extras key for bundle.
     */
    String EXTRA_BUNDLE = "com.optimumbrew.extra.Bundle";

    /**
     * To create the fragment.
     *
     * @param bundle The input argument bundle.
     * @return The created fragment.
     */
    BaseFragment createFragment(Bundle bundle);

    /**
     * To get the tag for fragment.
     *
     * @return The unique tag to identify fragment.
     */
    String getTag();
}
