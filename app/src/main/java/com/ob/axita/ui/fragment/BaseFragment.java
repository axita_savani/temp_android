package com.ob.axita.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ob.axita.R;
import com.ob.axita.ui.activity.BaseFragmentActivity;
import com.ob.axita.ui.interfaces.FragmentProvider;


/**
 * Created by Nirav Alagiya(nirav.alagiya@optimumbrew.com) on 02-Apr-15.
 */
public class BaseFragment extends Fragment implements FragmentProvider {

    protected BaseFragmentActivity baseActivity;
    private ProgressDialog progress;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.baseActivity = (BaseFragmentActivity) activity;
    }

    @Override
    public void onDetach() {
        this.baseActivity = null;
        super.onDetach();
    }


    public void setToolbarTitle(String title) {
        this.baseActivity.setToolbarTitle(title);
    }

    /*
     * To set the toolbar title.
     *
     * @param titleResId The string resource Id.
    */
    public void setToolbarTitle(int titleResId) {
        this.baseActivity.setToolbarTitle(titleResId);
    }

    public void hideToolbar() {
        this.baseActivity.hideToolbar();
    }

    public void enableSearchInToolbar(boolean status) {
        this.baseActivity.enableSearchInToolbar(status);
    }

    protected void showProgressBarWithoutHide() {
        if (progress == null) {
            progress = new ProgressDialog(getActivity());
            progress.setMessage(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.setCancelable(false);
            progress.show();
        } else if (!progress.isShowing()) {
            progress.show();
        }
    }


    protected void hideProgressBar() {
        if (progress != null) {
            progress.dismiss();
        }
    }

    public void showProgressBar() {
        hideProgressBar();
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();
    }


    @Override
    public BaseFragment createFragment(Bundle bundle) {
        return null;
    }


    @Override
    public void onDestroy() {
        hideProgressBar();
        super.onDestroy();
    }
}
