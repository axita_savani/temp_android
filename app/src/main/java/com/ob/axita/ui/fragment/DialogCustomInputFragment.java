package com.ob.axita.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ob.axita.R;
import com.ob.axita.ui.dialogfragment.customdialogfragment;
import com.ob.axita.ui.interfaces.HostInterface;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class DialogCustomInputFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "DialogCustomInput";
    private  static final int REQUEST_CODE=20;
    Context context;
    private HostInterface hostInterface;
    private Button btndialogcustom;
    private TextView txtname, txtpsd;
    Dialog dialog;
    String user, password, conpassword;
    private customdialogfragment customdialogfragment;

    @Override
    public void onAttach(Context con) {
        this.context = con;
        hostInterface = (HostInterface) context;
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Custom Dialog");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_custom_input, container, false);
        this.btndialogcustom = view.findViewById(R.id.btn_dialog_custom);
        this.txtname = view.findViewById(R.id.txt_dialog_user);
        this.txtpsd = view.findViewById(R.id.txt_dialog_pass);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btndialogcustom.setOnClickListener(this);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        Log.i(TAG, "----------Login click");
    showDialog();
    }

    private void showDialog() {
        FragmentManager fm=getFragmentManager();
        customdialogfragment=new customdialogfragment();
        customdialogfragment.setTargetFragment(this,REQUEST_CODE);
        Log.i(TAG,"dialog show");
        assert fm != null;
        customdialogfragment.show(fm,"Fragment dialog");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
Log.i(TAG,"onActivity Call");
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case REQUEST_CODE:
                if(resultCode == RESULT_OK)
                {
                    txtname.setText("Name : "+data.getStringExtra("Name"));
                    txtpsd.setText("Password : "+data.getStringExtra("password"));
                }
                if(resultCode==RESULT_CANCELED)
                {
                    Toast.makeText(context,"You not logined !!",Toast.LENGTH_LONG).show();
                }
        }
    }
}