package com.ob.axita.ui.activity;

//////////////////////////////////////////////////////////////////////////////
//                   OptimumBrew Technology Pvt. Ltd.                       //
//                                                                          //
// Title:            DeremaHelp                                             //
// File:             com.derema.deremahelp.ui.activity                      //
// Since:            11-August-2016                                         //
//                                                                          //
// Author:           Krishna Ghanva                                         //
// Email:            krishna.ghanva@optimumbrew.com                         //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ob.axita.R;
import com.ob.axita.core.constants.Extras;
import com.ob.axita.ui.fragment.BaseFragment;


public class BaseFragmentActivity extends AppCompatActivity {

    private static final String TAG = "BaseFragmentActivity";
    private boolean isStateSaved = false;

    public static boolean isSearchEnable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Log.e(TAG, "onCreate");
        setContentView(R.layout.activity_base);

        if (savedInstanceState != null) {
            isStateSaved = savedInstanceState.getBoolean("isStateSaved", false);
        } else {
            //Log.e(TAG,"** savedInstanceState is null **");
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BaseFragment fragment = (BaseFragment) getIntent().getSerializableExtra(Extras.EXTRA_FRAGMENT_SIGNUP);

        if (fragment != null) {

            Bundle bundle = getIntent().getBundleExtra(Extras.EXTRA_FRAGMENT_BUNDLE);
            fragment.setArguments(bundle);
            //Check if instance of activity is restored or it is new instance.
            Log.i(TAG, "current fragment: " + fragment.getClass().getName());
            ChangeCurrentFragment(fragment);

        } else {
            Log.e(TAG, "fragment is null");
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the state of activity
        outState.putBoolean("isStateSaved", true);
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState");
    }

    /**
     * @param fragment Set current fragment
     */
    private void ChangeCurrentFragment(Fragment fragment) {
        Log.e(TAG, "ChangeCurrentFragment");
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        fTransaction.setCustomAnimations(R.anim.fragment_enter_anim, R.anim.fragment_exit_anim);
        fTransaction.replace(R.id.layoutFHostFragment, fragment, fragment.getClass().getName());
        fTransaction.commit();
    }

    public void setToolbarTitle(String title) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

    }

    public void setToolbarTitle(int id) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(id);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_optionmenu,menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
       /* if (isSearchEnable) {
            menu.findItem(R.id.search).setVisible(true);
            isSearchEnable = false;
        } else {
            menu.findItem(R.id.search).setVisible(false);
        }*/
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void enableSearchInToolbar(boolean status) {

        isSearchEnable = true;
        invalidateOptionsMenu();
        //this.menu.findItem(R.id.search).setVisible(status);
    }


    public void hideToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "**onActivityResult()**");

        FragmentManager manager = getSupportFragmentManager();

       /* AllDealerFragment allDealerFragment = (AllDealerFragment) manager.findFragmentByTag(AllDealerFragment.class.getName());
        if (allDealerFragment != null) {
            allDealerFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.e(TAG, "AllDealerFragment is null");
        }*/
    }


    private void hideKeyBoard() {
        Log.i(TAG, "hideKeyBoard()");
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        hideKeyBoard();
        super.onBackPressed();
        Log.i(TAG, "onBackPressed()");
    }

    @Override
    protected void onDestroy() {
        hideKeyBoard();
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }

}
