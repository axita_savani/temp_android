package com.ob.axita.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;

import java.util.List;

/** Created by Razil on 27/07/2018. */
public class FilePickerFragment extends Fragment
        implements View.OnClickListener, FilePickerCallback {
    private static final String TAG = "FilePickerFragment";
    Context context;
    HostInterface hostInterface;
    private Button btnfilepicker;
    private TextView txtfilepathpicker;
    private FilePicker filePicker;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        hostInterface = (HostInterface) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("File Picker");
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file_picker, container, false);
        this.btnfilepicker = view.findViewById(R.id.btnFilePicker);
        this.txtfilepathpicker = view.findViewById(R.id.txtFilePathPicker);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnfilepicker.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFilePicker:
                Log.i(TAG, "FilePickerButtonclick");
                getFilePicker();
                break;
        }
    }

    private void getFilePicker() {
        Log.i(TAG, "getFilePicker()");
        filePicker = new FilePicker(this);
        filePicker.setFilePickerCallback(this);
        filePicker.pickFile();
    }

    @Override
    public void onFilesChosen(List<ChosenFile> list) {

        ChosenFile chosenFile = list.get(0);
        Log.i(TAG, "getFileExtensionFromMimeType()" + chosenFile.getFileExtensionFromMimeType());
        Log.i(TAG, "getOriginalPath()" + chosenFile.getOriginalPath());
        Log.i(TAG, "getMimeType()" + chosenFile.getMimeType());
        Log.i(TAG, "getFileExtensionFromMimeTypeWithoutDot()"
                        + chosenFile.getFileExtensionFromMimeTypeWithoutDot());
        txtfilepathpicker.setText(chosenFile.getOriginalPath());
    }

    @Override
    public void onError(String s) {}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            if (requestCode == Picker.PICK_FILE) {
                if (filePicker == null) {
                    filePicker = new FilePicker(this);
                    filePicker.setFilePickerCallback(this);
                    filePicker.pickFile();
                }
                filePicker.submit(data);
                Log.i(TAG, "DATA FILE-->" + data);
            }
        }
    }
}
