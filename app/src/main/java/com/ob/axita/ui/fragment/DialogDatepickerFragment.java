package com.ob.axita.ui.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;

import java.text.DateFormat;
import java.util.Calendar;

public class DialogDatepickerFragment extends Fragment implements View.OnClickListener {
    Context context;
    private HostInterface hostInterface;
    private Calendar calendar = Calendar.getInstance();
    private Button btndatepicker;
    private TextView txtdatepicker;
    public static final String TAG = "DialogDatepicke";

    ProgressDialog progressDialo;
    @Override
    public void onAttach(Context con) {
        this.context = con;
        hostInterface = (HostInterface) context;
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Date Picker");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_datepicker, container, false);
        this.btndatepicker = view.findViewById(R.id.btn_date_picker);
        this.txtdatepicker = view.findViewById(R.id.txt_date_picker);
        progressDialo=new ProgressDialog(context);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG,"onviewCreated");
        btndatepicker.setOnClickListener(this);

       /* progressDialo.setMax(100);
        progressDialo.setTitle("Loading...");
        progressDialo.setMessage("Ok....");
        progressDialo.setProgressStyle(1);
        progressDialo.setCancelable(true);
        progressDialo.incrementProgressBy(5);
        progressDialo.show();
        progressDialo.setProgress(0);

        if(progressDialo.getProgress()==100)
        {
            progressDialo.dismiss();
        }
*/

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String date = DateFormat.getDateInstance(DateFormat.MEDIUM).format(calendar.getTime());
                Log.i(TAG, "Selected date is " + date);
                txtdatepicker.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();

    }


}
