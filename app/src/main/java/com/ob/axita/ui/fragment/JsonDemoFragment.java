package com.ob.axita.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ob.axita.R;
import com.ob.axita.ui.adapter.JsonRecylerAdapter;
import com.ob.axita.ui.interfaces.HostInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;


public class JsonDemoFragment extends Fragment {

    private static final String TAG = "JsonDemoFragment";
    Context context;
    private HostInterface hostInterface;
    private RecyclerView jsonrecyclerView;
    private ArrayList<String> id = new ArrayList<>();
    private ArrayList<String> name = new ArrayList<>();
    private ArrayList<String> address = new ArrayList<>();
    private ArrayList<String> city = new ArrayList<>();
    private ArrayList<String> phone = new ArrayList<>();

    @Override
    public void onAttach(Context con) {
        this.context = con;
        hostInterface = (HostInterface) context;
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "On Create()");
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("JSON Display");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "Oncreate View()");
        View view = inflater.inflate(R.layout.fragment_json_demo, container, false);
        this.jsonrecyclerView = view.findViewById(R.id.json_recylerview);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "On View Create");
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        jsonrecyclerView.setLayoutManager(linearLayoutManager);
        try
        {
            JSONObject obj=new JSONObject(loadfromasset());
            Log.i(TAG,"json file catched");
            //Log.i(TAG,"json object"+obj);
            JSONArray   employeearray=obj.getJSONArray("Employee");
           // Log.i(TAG,"json Array"+employeearray);
            for(int i=0;i<employeearray.length();i++)
            {
                JSONObject empdetail=employeearray.getJSONObject(i);
             //   Log.i(TAG,"json get"+empdetail);
                id.add(empdetail.getString("id"));
                name.add(empdetail.getString("name"));
                address.add(empdetail.getString("address"));
                city.add(empdetail.getString("city"));
                phone.add(empdetail.getString("phone"));
            }

        }
        catch (JSONException e)
        {
e.printStackTrace();
        }
        JsonRecylerAdapter jsonRecylerAdapter=new JsonRecylerAdapter(id,name,address,city,phone,context);
        jsonrecyclerView.setAdapter(jsonRecylerAdapter);
        super.onViewCreated(view, savedInstanceState);
    }




    @Override
    public void onResume() {
        Log.i(TAG, "Onresume()");
        super.onResume();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Ondestroy()");
        super.onDestroy();
    }
    //json catched from file
    private String loadfromasset() {
        String json;
        try
        {
            InputStream is=getActivity().getAssets().open("myjson.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
                json = new String(buffer);
                Log.i(TAG," string json"+json+"--------");
        }
        catch (IOException ex)
        {
        ex.printStackTrace();
        return  null;
        }
        return  json;

    }
}
