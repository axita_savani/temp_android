package com.ob.axita.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.ob.axita.R;
import com.ob.axita.ui.activity.MainActivity;
import com.ob.axita.ui.interfaces.HostInterface;


public class PopupMenuFragment extends Fragment implements View.OnClickListener,PopupMenu.OnMenuItemClickListener {
    private static final String TAG ="PopupMenuFragment" ;
    Context context;
private HostInterface hostInterface;
private Button btnpopup;

    @Override
    public void onAttach(Context con) {
        this.context=con;
        hostInterface= (HostInterface) context;
        super.onAttach(context);

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Popup Menu");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

           View view=inflater.inflate(R.layout.fragment_popup_menu, container, false);
           this.btnpopup=view.findViewById(R.id.btn_popup);
           return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btnpopup.setOnClickListener(this);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        PopupMenu popup = new PopupMenu(context, view);
        popup.setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener) this);
        popup.inflate(R.menu.menu_popupmenu);
        Log.i(TAG,"gravity"+popup.getGravity());
        popup.setGravity(10);
        Log.i(TAG,"gravity"+popup.getGravity());
        popup.show();

    }
    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.popup_select:
                Toast.makeText(context,"Select One",Toast.LENGTH_SHORT).show();
                break;
            case R.id.popup_delete:
                Toast.makeText(context,"Delete One",Toast.LENGTH_SHORT).show();
                break;
            case R.id.popup_Deleteall:
                Toast.makeText(context,"Delete All",Toast.LENGTH_SHORT).show();
                break;
            case R.id.popup_Selectall:
                Toast.makeText(context,"Select All",Toast.LENGTH_SHORT).show();
                break;
            case R.id.popup_cancel:
                Toast.makeText(context,"Cancel",Toast.LENGTH_SHORT).show();
                break;
        }
    return  false;
    }
}
