package com.ob.axita.ui.interfaces;


import android.view.View;

public interface RecyclerViewItemInterface {
    /**
     * Called when the view is clicked.
     *
     * @param v           view that is clicked
     */
    void onItemClick(View v, long position);
    void onItemLongClick(View v, long position);

}
