package com.ob.axita.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ob.axita.R;
import com.ob.axita.core.constants.Extras;
import com.ob.axita.core.pojo.Item;
import com.ob.axita.ui.activity.BaseFragmentActivity;
import com.ob.axita.ui.adapter.ContactAdapter;
import com.ob.axita.ui.interfaces.HostInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ListViewContactFragment extends Fragment  {

    private static final String TAG ="ListViewContactFragment" ;
    private HostInterface hostInterface;
    private ListView listViewContact;
    private FloatingActionButton fab;
    ListView listView;
    private ArrayList<Item> contactarray;
    List<String> colors;
    private static ContactAdapter adapter;
    Random r = new Random();
    Context context;


    @SuppressWarnings("Annotator")


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        hostInterface = (HostInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getContext();
        hostInterface.setToolbarTitle("Contact");

    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        this.listViewContact = (ListView) view.findViewById(R.id.listViewContact);
        return view;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        colors = new ArrayList<String>();
        colors.add("#5E97F6");
        colors.add("#9CCC65");
        colors.add("#FF8A65");
        colors.add("#9E9E9E");
        colors.add("#9FA8DA");
        colors.add("#90A4AE");
        colors.add("#AED581");
        colors.add("#F6BF26");
        colors.add("#FFA726");
        contactarray = new ArrayList<Item>();


        int j = 0;
        for (int i = 0; i <= 1; i++) {
            j = r.nextInt(9 - 0);

        }


        contactarray.add(new Item("Axita", "7227006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Meera", "7227542155", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Mitul", "7227006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Hiral", "7287006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Rumana", "757006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Krishna", "9827006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Mitali", "9927006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Jesica", "7227256025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Khushbu", "8727876025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Axita", "7227006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Meera", "7227542155", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Mitul", "7227006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Hiral", "7287006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Rumana", "757006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Krishna", "9827006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Mitali", "9927006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Jesica", "7227256025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Khushbu", "8727876025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Khushbu", "8727876025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Axita", "7227006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Meera", "7227542155", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Mitul", "7227006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Hiral", "7287006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Rumana", "757006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Krishna", "9827006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Mitali", "9927006025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Jesica", "7227256025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));
        contactarray.add(new Item("Khushbu", "8727876025", Color.parseColor(colors.get(r.nextInt(9 - 0)))));


        adapter = new ContactAdapter(contactarray, context);
        Log.i(TAG,"contact"+getContext());
        LayoutInflater inflater = getLayoutInflater();
        //noinspection Annotator
        listViewContact.setAdapter(adapter);


       // fab.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
    }



}
