package com.ob.axita.ui.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ob.axita.R;


public class MainActivity extends AppCompatActivity {
    TextView textView;
Button btnlogin;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_main);
        btnlogin=findViewById(R.id.login);
        addListenerOnButton();
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SingUpActivity.class);
                startActivity(intent);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        SpannableStringBuilder str3 = new SpannableStringBuilder("Don't have an account ? JOIN NOW");
        str3.setSpan(clickableSpan2, 24, str3.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        str3.setSpan(new StyleSpan(Typeface.BOLD), 23, str3.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView = findViewById(R.id.ac);

        textView.setText(str3);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void addListenerOnButton() {
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
    }



}



