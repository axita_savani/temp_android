package com.ob.axita.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ob.axita.R;
import com.ob.axita.ui.interfaces.HostInterface;

import static android.support.constraint.Constraints.TAG;


public class DialogAlertFragment extends Fragment implements View.OnClickListener {
    Context context;
    private HostInterface hostInterface;

    private Button btndialogalert;
    public static final String TAG = "DialogAlertFragment";
    @Override
    public void onAttach(Context con) {
        this.context = con;
        hostInterface = (HostInterface) context;
        super.onAttach(context);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hostInterface.setToolbarTitle("Dialog Alert");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_alert, container, false);
        this.btndialogalert = view.findViewById(R.id.btn_dialog_alert);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btndialogalert.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
        alertdialog.setTitle("Alert Dialog?");
        alertdialog.setMessage("You want to change text of button ?");
        alertdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btndialogalert.setText("Clickable!!!!");
                Log.i(TAG,"-------------yes");
            }
        });
        alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btndialogalert.setText("Don't Click!!!");
                alertdialog.setCancelable(true);
                Log.i(TAG,"-------------no");
            }
        });
        alertdialog.show();
    }
}
